package cn.code4java.springbok.service;

import cn.code4java.springbok.utils.StringUtils;
import cn.code4java.springbok.vo.CecheIndexVO;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

/**
 * @ClassName CacheServiceImpl
 * @Description: 缓存服务实现类
 * @Author fengwensheng
 * @Date 2024/8/2
 * @Version V2.0.3
 **/
@Service
@AllArgsConstructor
public class CacheServiceImpl implements CacheService {

    private RedisTemplate<String, String> redisTemplate;

    @Override
    public CecheIndexVO listCacheIndex() {
        CecheIndexVO cacheIndexVO = new CecheIndexVO();
        Properties execute = (Properties) redisTemplate.execute((RedisCallback<Object>) action -> action.info());
        cacheIndexVO.setRedisVersion(execute.getProperty("redis_version")); // 版本号
        cacheIndexVO.setUptimeInDays(execute.getProperty("uptime_in_days")); // Redis启动后的天数
        cacheIndexVO.setConnectedClients(execute.getProperty("connected_clients")); // 当前客户端连接数
        cacheIndexVO.setUsedMemoryHuman(execute.getProperty("used_memory_human")); // 已使用内存
        BigDecimal usedMemoryDataset = new BigDecimal(execute.getProperty("used_memory_dataset"))
                .divide(new BigDecimal(1024)).setScale(2, RoundingMode.DOWN);
        cacheIndexVO.setUsedMemoryDataset(usedMemoryDataset.toString() + "K"); // Redis保存数据集的大小
        cacheIndexVO.setTotalSystemMemoryHuman(execute.getProperty("total_system_memory_human")); // 当前总内存
        // RDB
        cacheIndexVO.setRdbBgsaveInProgress(execute.getProperty("rdb_bgsave_in_progress")); // 当前RDB的进行状态
        LocalDate rdbLastSaveTime = Instant.ofEpochMilli(Long.parseLong(execute.getProperty("rdb_last_save_time"))).atZone(ZoneId.systemDefault()).toLocalDate();
        cacheIndexVO.setRdbLastSaveTime(rdbLastSaveTime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"))); // 上次成功完成的RDB时间戳
        cacheIndexVO.setRdbLastBgsaveStatu(execute.getProperty("rdb_last_bgsave_statu")); // 上次RDB的状态
        cacheIndexVO.setRdbLastBgsaveTimeSec(execute.getProperty("rdb_last_bgsave_time_sec")); // 上次RDB保存用时（秒）
        cacheIndexVO.setRdbSaves(execute.getProperty("rdb_saves")); // 启动以来执行RDB快照的次数
        // AOF
        cacheIndexVO.setAofEnabled(execute.getProperty("aof_enabled")); // 是否开启AOF
        cacheIndexVO.setAofRewireInProgress(execute.getProperty("aof_rewire_in_progress")); // 正在进行AOF重写的状态
        cacheIndexVO.setAofLastRewireTimeSec(execute.getProperty("aof_last_rewire_time_sec")); // 上次AOF重写用时（秒）
        cacheIndexVO.setAofLastBgrewriteStatus(execute.getProperty("aof_last_bgrewrite_status")); // 上次AOF重写的状态
        BigDecimal aofCurrentSize = BigDecimal.ZERO;
        if (StringUtils.isNotBlank(execute.getProperty("aof_current_size"))) {
            aofCurrentSize = new BigDecimal(execute.getProperty("aof_current_size"))
                    .divide(new BigDecimal(1024)).setScale(2, RoundingMode.DOWN);
        }
        cacheIndexVO.setAofCurrentSize(aofCurrentSize + "K"); // AOF当前文件大小
        cacheIndexVO.setAofRewrites(execute.getProperty("aof_rewrites")); // 启动以来执行AOF重写的次数
        return cacheIndexVO;
    }
}
