package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.SysBranchQueryDTO;
import cn.code4java.springbok.entity.SysBranch;
import cn.code4java.springbok.mapper.SysBranchMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

/**
 * @ClassName SysBranchServiceImpl
 * @Description: 门店实现类
 * @Author xiehuangbao
 * @Date 2024/6/15
 * @Version V1.0
 **/
@Service
public class SysBranchServiceImpl extends BaseServiceImpl<SysBranchMapper, SysBranch> implements SysBranchService {

    /**
     * 分页查询门店
     *
     * @param sysBranchQueryDTO
     * @return
     */
    @Override
    public Page<SysBranch> pageSysBranch(SysBranchQueryDTO sysBranchQueryDTO) {
        return this.baseMapper.pageSysBranch(getPage(), sysBranchQueryDTO);
    }
}
