package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.SysAppQueryDTO;
import cn.code4java.springbok.entity.SysApp;
import cn.code4java.springbok.mapper.SysAppMapper;
import cn.code4java.springbok.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

/**
 * @ClassName SysAppServiceImpl
 * @Description: 应用服务实现类
 * @Author fengwensheng
 * @Date 2024/2/23
 * @Version V1.0
 **/
@Service
public class SysAppServiceImpl extends BaseServiceImpl<SysAppMapper, SysApp> implements SysAppService {

    @Override
    public Page<SysApp> pageSysApp(SysAppQueryDTO params) {
        LambdaQueryWrapper<SysApp> wrapper = Wrappers.<SysApp>lambdaQuery()
                .like(StringUtils.isNotBlank(params.getAppId()), SysApp::getAppId, params.getAppId())
                .ge(params.getStartTime() != null, SysApp::getCreatedTime, params.getStartTime())
                .le(params.getEndTime() != null, SysApp::getCreatedTime, params.getEndTime());
        return this.page(getPage(), wrapper);
    }
}
