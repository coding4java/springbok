package cn.code4java.springbok.controller;

import cn.code4java.springbok.service.CacheService;
import cn.code4java.springbok.vo.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName CecheController
 * @Description: 缓存控制器
 * @Author fengwensheng
 * @Date 2024/8/2
 * @Version V2.0.3
 **/
@Tag(name = "缓存管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/cache")
public class CecheController {

    private final CacheService cacheService;

    /**
     * 查询缓存指标
     *
     * @return
     */
    @GetMapping("/listCacheIndex")
    @Operation(summary = "查询缓存指标", description = "查询缓存指标")
    public BaseResponse listCacheIndex() {
        return BaseResponse.success(cacheService.listCacheIndex());
    }
}
