package cn.code4java.springbok.controller;

import cn.code4java.springbok.dto.TableColumnDTO;
import cn.code4java.springbok.entity.TableColumn;
import cn.code4java.springbok.service.TableColumnService;
import cn.code4java.springbok.vo.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * @ClassName TableColumnController
 * @Description: 生成表字段
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
@Tag(name = "应用管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/tableColumn")
public class TableColumnController {

    private final TableColumnService tableColumnService;

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @GetMapping("/pageTableColumn")
    @Operation(summary = "分页查询", description = "分页查询")
    public BaseResponse pageTableColumn(TableColumnDTO params) {
        return BaseResponse.success(tableColumnService.pageTableColumn(params));
    }

    /**
     * 新增
     *
     * @param tableColumn
     * @return
     */
    @PostMapping("/addTableColumn")
    public BaseResponse addTableColumn(@RequestBody TableColumn tableColumn) {
        return BaseResponse.success(tableColumnService.save(tableColumn));
    }

    /**
     * 修改
     *
     * @param tableColumn
     * @return
     */
    @PostMapping("/updateTableColumn")
    public BaseResponse updateTableColumn(@RequestBody TableColumn tableColumn) {
        return BaseResponse.success(tableColumnService.updateById(tableColumn));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteTableColumn")
    public BaseResponse deleteTableColumn(@Parameter(description = "id", required = true) Integer id) {
        return BaseResponse.success(tableColumnService.removeById(id));
    }
}
