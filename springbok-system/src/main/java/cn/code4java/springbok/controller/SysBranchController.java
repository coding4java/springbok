package cn.code4java.springbok.controller;

import cn.code4java.springbok.annotation.Log;
import cn.code4java.springbok.dto.SysBranchQueryDTO;
import cn.code4java.springbok.entity.SysBranch;
import cn.code4java.springbok.service.SysBranchService;
import cn.code4java.springbok.vo.BaseResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName SysBranchController
 * @Description: 门店管理控制器
 * @Author xiehuangbao
 * @Date 2024/6/15
 * @Version V1.0
 **/
@Tag(name = "门店管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/branch")
public class SysBranchController {

    private final SysBranchService sysBranchService;

    /**
     * 分页查询门店信息
     *
     * @param params
     * @return
     */
    @GetMapping("/pageSysBranch")
    @Operation(summary = "分页查询门店信息", description = "分页查询门店信息")
    public BaseResponse pageSysBranch(SysBranchQueryDTO params) {
        return BaseResponse.success(sysBranchService.pageSysBranch(params));
    }

    /**
     * 新增门店
     *
     * @param sysBranch
     * @return
     */
    @Log(title = "新增门店")
    @PostMapping("/addSysBranch")
    @Operation(summary = "新增门店", description = "新增门店")
    public BaseResponse addSysBranch(@RequestBody SysBranch sysBranch) {
        return BaseResponse.success(sysBranchService.save(sysBranch));
    }

    /**
     * 修改门店
     *
     * @param sysBranch
     * @return
     */
    @Log(title = "修改门店")
    @PostMapping("/updateSysBranch")
    @Operation(summary = "修改门店", description = "修改门店")
    public BaseResponse updateSysBranch(@RequestBody SysBranch sysBranch) {
        return BaseResponse.success(sysBranchService.updateById(sysBranch));
    }

    /**
     * 删除门店
     *
     * @param sysBranchId
     * @return
     */
    @Log(title = "删除门店")
    @PostMapping("/deleteSysBranch")
    @Operation(summary = "删除门店", description = "删除门店")
    public BaseResponse deleteSysBranch(@Parameter(description = "门店id", required = true) Integer sysBranchId) {
        return BaseResponse.success(sysBranchService.removeById(sysBranchId));
    }

    /**
     * 开启关闭门店
     *
     * @param sysBranchId
     * @param branchStatus
     * @return
     */
    @Log(title = "开启关闭门店")
    @PostMapping("/enabledSysBranch")
    @Operation(summary = "开启关闭门店", description = "开启关闭门店")
    public BaseResponse enabledSysBranch(@Parameter(description = "id", required = true) @RequestParam("sysBranchId") Integer sysBranchId,
                                      @Parameter(description = "状态 1：营业 2：闭店", required = true) @RequestParam("branchStatus")Integer branchStatus) {
        SysBranch branch = sysBranchService.getById(sysBranchId);
        branch.setBranchStatus(branchStatus);
        sysBranchService.updateById(branch);
        return BaseResponse.success();
    }

    /**
     * 查询门店信息列表
     *
     * @param params
     * @return
     */
    @GetMapping("/listSysBranch")
    @Operation(summary = "查询门店信息列表", description = "查询门店信息列表")
    public BaseResponse listSysBranch(SysBranchQueryDTO params) {
        LambdaQueryWrapper<SysBranch> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(SysBranch::getBranchCode, params.getBranchName()).or().like(SysBranch::getBranchName, params.getBranchName());
        return BaseResponse.success(sysBranchService.list(queryWrapper));
    }
}
