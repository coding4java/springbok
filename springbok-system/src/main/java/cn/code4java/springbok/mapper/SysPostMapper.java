package cn.code4java.springbok.mapper;

import cn.code4java.springbok.entity.SysPost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @ClassName SysPostMapper
 * @Description: SysPostMapper
 * @Author fengwensheng
 * @Date 2024/03/13
 * @Version V1.0
 **/
public interface SysPostMapper extends BaseMapper<SysPost> {
}
