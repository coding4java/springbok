/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : springbok

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2024-07-16 11:46:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for erp_inventory_order
-- ----------------------------
DROP TABLE IF EXISTS `erp_inventory_order`;
CREATE TABLE `erp_inventory_order` (
  `inventory_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(255) DEFAULT NULL,
  `sys_branch_id` int(11) DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL,
  `audit_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`inventory_order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_inventory_order
-- ----------------------------

-- ----------------------------
-- Table structure for erp_inventory_order_line
-- ----------------------------
DROP TABLE IF EXISTS `erp_inventory_order_line`;
CREATE TABLE `erp_inventory_order_line` (
  `inventory_order_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_order_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `real_quantity` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`inventory_order_line_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_inventory_order_line
-- ----------------------------

-- ----------------------------
-- Table structure for erp_purchase_in_order
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase_in_order`;
CREATE TABLE `erp_purchase_in_order` (
  `purchase_in_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_branch_id` int(11) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `order_amount` decimal(10,2) DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL,
  `audit_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`purchase_in_order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_purchase_in_order
-- ----------------------------

-- ----------------------------
-- Table structure for erp_purchase_in_order_line
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase_in_order_line`;
CREATE TABLE `erp_purchase_in_order_line` (
  `purchase_in_order_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_in_order_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`purchase_in_order_line_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_purchase_in_order_line
-- ----------------------------

-- ----------------------------
-- Table structure for erp_purchase_out_order
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase_out_order`;
CREATE TABLE `erp_purchase_out_order` (
  `purchase_out_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_branch_id` int(11) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `order_amount` decimal(10,2) DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL,
  `audit_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`purchase_out_order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_purchase_out_order
-- ----------------------------

-- ----------------------------
-- Table structure for erp_purchase_out_order_line
-- ----------------------------
DROP TABLE IF EXISTS `erp_purchase_out_order_line`;
CREATE TABLE `erp_purchase_out_order_line` (
  `purchase_out_order_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_out_order_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`purchase_out_order_line_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_purchase_out_order_line
-- ----------------------------

-- ----------------------------
-- Table structure for erp_stock
-- ----------------------------
DROP TABLE IF EXISTS `erp_stock`;
CREATE TABLE `erp_stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_branch_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `cost_amount` decimal(10,2) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`stock_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_stock
-- ----------------------------

-- ----------------------------
-- Table structure for erp_stock_line
-- ----------------------------
DROP TABLE IF EXISTS `erp_stock_line`;
CREATE TABLE `erp_stock_line` (
  `stock_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`stock_line_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of erp_stock_line
-- ----------------------------

-- ----------------------------
-- Table structure for mall_banner
-- ----------------------------
DROP TABLE IF EXISTS `mall_banner`;
CREATE TABLE `mall_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `to_page` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_coupon
-- ----------------------------
DROP TABLE IF EXISTS `mall_coupon`;
CREATE TABLE `mall_coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_name` varchar(255) DEFAULT NULL,
  `coupon_description` varchar(255) DEFAULT NULL,
  `coupon_image` varchar(255) DEFAULT NULL,
  `coupon_type` int(1) DEFAULT NULL,
  `coupon_limit` decimal(10,2) DEFAULT NULL,
  `coupon_amount` decimal(10,2) DEFAULT NULL,
  `used_item_sale_type` int(1) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`coupon_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_coupon_direct
-- ----------------------------
DROP TABLE IF EXISTS `mall_coupon_direct`;
CREATE TABLE `mall_coupon_direct` (
  `coupon_direct_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `coupon_direct_name` varchar(255) DEFAULT NULL COMMENT '发券名称',
  `send_type` tinyint(1) DEFAULT NULL COMMENT '发放类型 1 按标签 2 指定手机号',
  `send_target_ids` text COMMENT '发放目标id',
  `sms_notify` tinyint(1) DEFAULT NULL COMMENT '是否启用短信通知 0 否 1 是',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0 未开始 1 发送中 2 已完成 3 发送失败',
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`coupon_direct_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_coupon_direct
-- ----------------------------

-- ----------------------------
-- Table structure for mall_coupon_direct_line
-- ----------------------------
DROP TABLE IF EXISTS `mall_coupon_direct_line`;
CREATE TABLE `mall_coupon_direct_line` (
  `coupon_direct_line_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `coupon_direct_id` int(11) DEFAULT NULL COMMENT '定向发券id',
  `coupon_id` int(11) DEFAULT NULL COMMENT '券id',
  `send_num` int(11) DEFAULT NULL COMMENT '发放数量',
  `validity_type` tinyint(1) DEFAULT NULL COMMENT '有效期类型 0 指定日期 1 指定周期',
  `start_date` date DEFAULT NULL COMMENT '开始日期',
  `end_date` date DEFAULT NULL COMMENT '结束日期',
  `after_day_num` int(11) DEFAULT NULL COMMENT '发券后N天',
  `valid_day_num` int(11) DEFAULT NULL COMMENT '有效天数',
  PRIMARY KEY (`coupon_direct_line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mall_coupon_direct_line
-- ----------------------------

-- ----------------------------
-- Table structure for mall_coupon_item_sale
-- ----------------------------
DROP TABLE IF EXISTS `mall_coupon_item_sale`;
CREATE TABLE `mall_coupon_item_sale` (
  `coupon_item_sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`coupon_item_sale_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_item
-- ----------------------------
DROP TABLE IF EXISTS `mall_item`;
CREATE TABLE `mall_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `item_code` varchar(255) NOT NULL COMMENT '商品编码',
  `item_name` varchar(255) NOT NULL COMMENT '商品名称',
  `item_unit_name` varchar(25) NOT NULL COMMENT '基础单位名称',
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mall_item
-- ----------------------------
INSERT INTO `mall_item` VALUES ('2', '10010', 'A级-金腰榴莲', 'g', 'admin', '2024-07-13 09:35:55', 'admin', '2024-07-13 10:30:49');
INSERT INTO `mall_item` VALUES ('3', '10011', 'A级-金枕榴莲', 'g', 'admin', '2024-07-13 10:31:26', null, null);
INSERT INTO `mall_item` VALUES ('4', '10012', 'A级-猫山王榴莲', 'g', 'admin', '2024-07-13 10:32:08', null, null);
INSERT INTO `mall_item` VALUES ('5', '10013', 'A级-甘美西瓜', 'g', 'admin', '2024-07-13 10:34:34', null, null);
INSERT INTO `mall_item` VALUES ('6', '10014', 'B级-甘美西瓜', 'g', 'admin', '2024-07-13 10:34:44', 'admin', '2024-07-13 10:34:57');
INSERT INTO `mall_item` VALUES ('7', '10015', 'A级-墨童西瓜', 'g', 'admin', '2024-07-13 10:35:25', null, null);
INSERT INTO `mall_item` VALUES ('8', '10016', 'A级-蜜童西瓜', 'g', 'admin', '2024-07-13 10:35:45', null, null);
INSERT INTO `mall_item` VALUES ('9', '10017', 'A级-美月西瓜', 'g', 'admin', '2024-07-13 10:36:03', null, null);
INSERT INTO `mall_item` VALUES ('10', '10018', 'A级-甜王西瓜', 'g', 'admin', '2024-07-13 10:36:21', null, null);
INSERT INTO `mall_item` VALUES ('11', '10019', 'A级-冰淇淋西瓜', 'g', 'admin', '2024-07-13 10:36:42', null, null);
INSERT INTO `mall_item` VALUES ('12', '10020', 'A级-小糖丸西瓜', 'g', 'admin', '2024-07-13 10:37:01', null, null);
INSERT INTO `mall_item` VALUES ('13', '10021', 'B级-小糖丸西瓜', 'g', 'admin', '2024-07-13 10:37:09', null, null);
INSERT INTO `mall_item` VALUES ('14', '10022', 'A级-麒麟西瓜', 'g', 'admin', '2024-07-13 10:37:32', null, null);
INSERT INTO `mall_item` VALUES ('15', '10023', 'A级-火焰无籽葡萄', 'g', 'admin', '2024-07-13 10:38:18', null, null);
INSERT INTO `mall_item` VALUES ('16', '10024', 'A级-峰后葡萄', 'g', 'admin', '2024-07-13 10:38:34', 'admin', '2024-07-13 10:38:42');
INSERT INTO `mall_item` VALUES ('17', '10025', 'A级-冰糖葡萄', 'g', 'admin', '2024-07-13 10:39:08', null, null);
INSERT INTO `mall_item` VALUES ('18', '10026', 'A级-夏黑葡萄', 'g', 'admin', '2024-07-13 10:39:26', null, null);
INSERT INTO `mall_item` VALUES ('19', '10027', 'A级-水晶葡萄', 'g', 'admin', '2024-07-13 10:39:41', null, null);
INSERT INTO `mall_item` VALUES ('20', '10028', 'A级-红巨峰葡萄', 'g', 'admin', '2024-07-13 10:40:07', null, null);
INSERT INTO `mall_item` VALUES ('21', '10029', 'A级-黑宝石葡萄', 'g', 'admin', '2024-07-13 10:40:22', null, null);
INSERT INTO `mall_item` VALUES ('22', '10030', 'A级-进口黑加伦葡萄', 'g', 'admin', '2024-07-13 10:40:54', null, null);
INSERT INTO `mall_item` VALUES ('23', '10031', 'A级-慕斯卡香水葡萄', 'g', 'admin', '2024-07-13 10:41:34', null, null);
INSERT INTO `mall_item` VALUES ('24', '10032', 'A级-巨峰葡萄', 'g', 'admin', '2024-07-13 10:42:09', null, null);
INSERT INTO `mall_item` VALUES ('25', '10033', 'A级-香印青提', 'g', 'admin', '2024-07-13 10:43:16', null, null);
INSERT INTO `mall_item` VALUES ('26', '10034', 'A级-进口无籽青提', 'g', 'admin', '2024-07-13 10:43:24', null, null);
INSERT INTO `mall_item` VALUES ('27', '10035', 'A级-阳光玫瑰青提', 'g', 'admin', '2024-07-13 10:43:55', null, null);
INSERT INTO `mall_item` VALUES ('28', '10036', 'A级-晴王葡萄', 'g', 'admin', '2024-07-13 10:44:59', null, null);
INSERT INTO `mall_item` VALUES ('29', '10037', 'A级-黑钻车厘子', 'g', 'admin', '2024-07-13 10:46:34', null, null);
INSERT INTO `mall_item` VALUES ('30', '10038', 'A级-进口车厘子', 'g', 'admin', '2024-07-13 10:46:50', null, null);
INSERT INTO `mall_item` VALUES ('31', '10039', '比比赞日式小圆饼干100g', '包', 'admin', '2024-07-13 10:49:09', 'admin', '2024-07-13 10:52:51');
INSERT INTO `mall_item` VALUES ('32', '10040', '四洲脆脆虾饼袋装15g', '袋', 'admin', '2024-07-13 10:50:23', 'admin', '2024-07-13 10:52:45');
INSERT INTO `mall_item` VALUES ('33', '10041', '伊利纯牛奶250ml', '盒', 'admin', '2024-07-13 10:50:53', 'admin', '2024-07-13 10:52:41');
INSERT INTO `mall_item` VALUES ('34', '10041', '卡士酸牛乳草莓味120g', '杯', 'admin', '2024-07-13 10:51:42', null, null);
INSERT INTO `mall_item` VALUES ('35', '10042', '可口可乐纤细版330ml', '瓶', 'admin', '2024-07-13 10:52:37', null, null);
INSERT INTO `mall_item` VALUES ('36', '10043', '日本梅乃宿梅酒12度720mL', '瓶', 'admin', '2024-07-15 15:55:04', null, null);
INSERT INTO `mall_item` VALUES ('37', '10044', 'A级-小菠萝', 'g', 'admin', '2024-07-16 11:04:17', null, null);

-- ----------------------------
-- Table structure for mall_item_sale
-- ----------------------------
DROP TABLE IF EXISTS `mall_item_sale`;
CREATE TABLE `mall_item_sale` (
  `item_sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_sale_code` varchar(255) DEFAULT NULL,
  `item_sale_name` varchar(255) DEFAULT NULL,
  `main_image` varchar(4096) DEFAULT NULL,
  `item_image` varchar(1020) DEFAULT NULL,
  `item_sale_description` text,
  `item_sale_price` decimal(10,2) DEFAULT NULL,
  `item_sale_class_code` int(11) DEFAULT NULL,
  `spec_json` varchar(4096) DEFAULT NULL,
  `property_json` varchar(4096) DEFAULT NULL,
  `tag_ids` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_sale_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=651 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_item_sale_channel
-- ----------------------------
DROP TABLE IF EXISTS `mall_item_sale_channel`;
CREATE TABLE `mall_item_sale_channel` (
  `item_sale_channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_branch_id` int(11) DEFAULT NULL,
  `item_sale_id` int(11) DEFAULT NULL,
  `item_sale_channel_name` varchar(255) DEFAULT NULL,
  `sale_channel` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_sale_channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for mall_item_sale_class
-- ----------------------------
DROP TABLE IF EXISTS `mall_item_sale_class`;
CREATE TABLE `mall_item_sale_class` (
  `item_sale_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_sale_class_code` varchar(255) DEFAULT NULL,
  `item_sale_class_name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `class_icon` varchar(255) DEFAULT NULL,
  `show_index` tinyint(1) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_sale_class_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_item_sale_class
-- ----------------------------
INSERT INTO `mall_item_sale_class` VALUES ('247', '1005000', '水果鲜花', '0', 'http://localhost:9988/image/system/f7512c26-733a-430d-be8c-2a20e87859d0.png', '1', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 22:42:13');
INSERT INTO `mall_item_sale_class` VALUES ('248', '1005999003', '西瓜/蜜瓜', '247', 'http://localhost:9988/image/system/df8f27d3-5fd8-4c1a-8a3e-54a4ca050275.png', '0', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 09:44:53');
INSERT INTO `mall_item_sale_class` VALUES ('249', '1008017', '榴莲/热带果', '247', 'http://localhost:9988/image/system/8a51ac1e-4b6f-426e-8637-e67659b511a0.png', '0', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 09:45:17');
INSERT INTO `mall_item_sale_class` VALUES ('250', '1017000', '葡萄/提子', '247', 'http://localhost:9988/image/system/459f8197-586d-43cc-8f55-168f2ac40966.png', '0', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 09:45:11');
INSERT INTO `mall_item_sale_class` VALUES ('251', '109243003', '苹果/梨/蕉', '247', 'http://localhost:9988/image/system/55ac5a61-b05e-4371-bec2-1ddd77396a15.png', '0', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 09:45:39');
INSERT INTO `mall_item_sale_class` VALUES ('252', '109248004', '莓果/猕猴桃', '247', 'http://localhost:9988/image/system/59b120c8-36f1-4ee5-8510-5a16259d354f.png', '0', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 09:45:56');
INSERT INTO `mall_item_sale_class` VALUES ('253', '109293000', '果切', '247', 'http://localhost:9988/image/system/e14351d0-235a-40c5-8263-b42134b4d1b3.png', '0', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 09:46:03');
INSERT INTO `mall_item_sale_class` VALUES ('255', '1005002', '海鲜水产', '0', 'http://localhost:9988/image/system/a81f42cd-946a-47be-95b2-0200aa99b281.png', '1', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 22:42:02');
INSERT INTO `mall_item_sale_class` VALUES ('256', '1005012', '小龙虾/虾', '255', 'http://localhost:9988/image/system/9e6ea977-13ef-400b-a948-538bb7d97a1c.png', '0', 'admin', '2024-01-27 12:59:19', 'admin', '2024-07-16 09:55:19');
INSERT INTO `mall_item_sale_class` VALUES ('257', '1036003', '多宝鱼/鱼', '255', 'http://localhost:9988/image/system/09296acc-5991-4fdb-9a2e-1efa46e390eb.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:55:27');
INSERT INTO `mall_item_sale_class` VALUES ('258', '109201001', '六月黄/蟹', '255', 'http://localhost:9988/image/system/2e93b634-b54c-4ba6-b426-ef8dd339add5.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:55:37');
INSERT INTO `mall_item_sale_class` VALUES ('259', '109206007', '刺身/三文鱼', '255', 'http://localhost:9988/image/system/b60f7504-0932-4c38-a7a1-8d5b1e2d671b.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:55:51');
INSERT INTO `mall_item_sale_class` VALUES ('260', '109264007', '鲜活水产', '255', 'http://localhost:9988/image/system/a220fd02-64ba-4942-b70c-59b6b7d42e70.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:56:03');
INSERT INTO `mall_item_sale_class` VALUES ('261', '109285003', '冰鲜水产', '255', 'http://localhost:9988/image/system/f11798b4-8550-488a-88ce-7733dc7e12b1.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:56:10');
INSERT INTO `mall_item_sale_class` VALUES ('262', '1010000', '休闲零食', '0', 'http://localhost:9988/image/system/b6741b16-fd8f-4072-bbd0-3663fec08d6d.png', '1', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 22:42:59');
INSERT INTO `mall_item_sale_class` VALUES ('263', '109303000', '膨化薯片', '262', 'http://localhost:9988/image/system/922898a1-9708-40aa-a01e-b655da9fea9a.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:48:30');
INSERT INTO `mall_item_sale_class` VALUES ('264', '109311005', '辣条/海产', '262', 'http://localhost:9988/image/system/89e0e0b1-04b2-43d8-ad40-12f5db8053e2.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:48:48');
INSERT INTO `mall_item_sale_class` VALUES ('265', '109311006', '肉干/凤爪', '262', 'http://localhost:9988/image/system/a68b0780-d6c0-48ea-8b4b-83dfafff0793.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:48:58');
INSERT INTO `mall_item_sale_class` VALUES ('266', '109311007', '糕点/面包', '262', 'http://localhost:9988/image/system/29a324ad-6290-4a2a-a382-7319df9f8965.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:49:08');
INSERT INTO `mall_item_sale_class` VALUES ('267', '109315000', '饼干/曲奇', '262', 'http://localhost:9988/image/system/40425d63-c345-4169-8ae0-07a42892a171.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:49:16');
INSERT INTO `mall_item_sale_class` VALUES ('268', '109318003', '坚果/瓜子', '262', 'http://localhost:9988/image/system/9fea990c-acf6-45d1-aec7-ae5423378319.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:49:25');
INSERT INTO `mall_item_sale_class` VALUES ('269', '1011000', '蔬菜豆制品', '0', 'http://localhost:9988/image/system/083cf01f-97bb-4c20-89f2-da80f9bd720f.png', '1', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 22:41:41');
INSERT INTO `mall_item_sale_class` VALUES ('270', '1020003', '番茄/茄瓜', '269', 'http://localhost:9988/image/system/c28c279c-7da3-411e-b918-d6497d3414aa.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:50:44');
INSERT INTO `mall_item_sale_class` VALUES ('271', '1037006', '生菜/叶菜', '269', 'http://localhost:9988/image/system/07004571-4cc6-4fc5-8271-a6da1feefca6.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:50:54');
INSERT INTO `mall_item_sale_class` VALUES ('272', '109243018', '辣椒/椒类', '269', 'http://localhost:9988/image/system/3f488359-1c3d-4331-9885-511299f2557b.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:51:15');
INSERT INTO `mall_item_sale_class` VALUES ('273', '109243019', '土豆/根茎', '269', 'http://localhost:9988/image/system/b0d5544d-aaba-4917-a939-09b24d06be0e.png', '0', 'admin', '2024-01-27 12:59:20', 'admin', '2024-07-16 09:51:27');
INSERT INTO `mall_item_sale_class` VALUES ('274', '109243021', '香菇/菌菇', '269', 'http://localhost:9988/image/system/49e6bdd8-ab21-401c-afb0-2225d84388e5.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:51:38');
INSERT INTO `mall_item_sale_class` VALUES ('275', '109243022', '葱姜蒜/调味', '269', 'http://localhost:9988/image/system/2b8d25cc-62db-4021-99c1-5988245c5a44.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:51:50');
INSERT INTO `mall_item_sale_class` VALUES ('276', '1013001', '乳品烘焙', '0', 'http://localhost:9988/image/system/b38a4823-055d-4af9-a9d6-7e2d472d467b.png', '1', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 22:42:23');
INSERT INTO `mall_item_sale_class` VALUES ('277', '1009000', '常温奶/箱装', '276', 'http://localhost:9988/image/system/7d7b5206-dbff-4acd-b0da-c0c79f0ee6e2.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:52:24');
INSERT INTO `mall_item_sale_class` VALUES ('278', '1020002', '鲜奶/豆浆', '276', 'http://localhost:9988/image/system/7c8cc076-3bff-4b29-8463-2906b99f136a.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:52:32');
INSERT INTO `mall_item_sale_class` VALUES ('279', '109243016', '冷藏酸奶', '276', 'http://localhost:9988/image/system/3252cb15-4a89-4a35-8a47-db3832068dac.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:52:41');
INSERT INTO `mall_item_sale_class` VALUES ('280', '109256012', '吐司/三明治', '276', 'http://localhost:9988/image/system/68425841-13ee-4416-9081-6408c7517570.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:52:49');
INSERT INTO `mall_item_sale_class` VALUES ('281', '109256013', '月饼/麻薯', '276', 'http://localhost:9988/image/system/d5e2bb37-1fa3-4a2a-b893-3f3e0e5eb6fb.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:52:57');
INSERT INTO `mall_item_sale_class` VALUES ('282', '109261055', '奶酪/黄油', '276', 'http://localhost:9988/image/system/8c20611f-2194-4cd1-9caa-d7a649031c0b.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:53:09');
INSERT INTO `mall_item_sale_class` VALUES ('283', '1019000', '肉禽蛋', '0', 'http://localhost:9988/image/system/02fff533-47c1-436d-b9f9-1739b29cdbc0.png', '1', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 22:41:52');
INSERT INTO `mall_item_sale_class` VALUES ('284', '1065004', '鸡鸭鸽', '283', 'http://localhost:9988/image/system/831d5525-ebc1-4c23-b3af-66657da437c1.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:56:56');
INSERT INTO `mall_item_sale_class` VALUES ('285', '109256014', '猪肉', '283', 'http://localhost:9988/image/system/0bfd7865-79b7-4595-b550-08ed6ac31b19.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:57:00');
INSERT INTO `mall_item_sale_class` VALUES ('286', '109256015', '牛肉', '283', 'http://localhost:9988/image/system/61db9ea7-c7b9-4760-8320-eb65cf42d239.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:57:06');
INSERT INTO `mall_item_sale_class` VALUES ('287', '109275000', '羊肉', '283', 'http://localhost:9988/image/system/ad43cac7-61df-4f64-8add-d70e480a4ace.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:57:11');
INSERT INTO `mall_item_sale_class` VALUES ('288', '109309012', '腊肉/腊肠', '283', 'http://localhost:9988/image/system/71aab2e3-724b-453c-9417-96e87e6b010d.png', '0', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 09:57:22');
INSERT INTO `mall_item_sale_class` VALUES ('289', '1043000', '熟食快手菜', '0', 'http://localhost:9988/image/system/1aa412de-6bbb-4b2d-87d5-1292f4ab6e78.png', '1', 'admin', '2024-01-27 12:59:21', 'admin', '2024-07-16 22:43:19');
INSERT INTO `mall_item_sale_class` VALUES ('290', '1008006', '熟食卤味', '289', 'http://localhost:9988/image/system/a8fc79b9-f578-4470-af6f-eeb56d22cfc2.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:57:46');
INSERT INTO `mall_item_sale_class` VALUES ('291', '1022000', '培根/香肠', '289', 'http://localhost:9988/image/system/717386ce-c7f8-479f-84bd-1158928b46f4.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:58:04');
INSERT INTO `mall_item_sale_class` VALUES ('292', '1028001', '烧烤食材', '289', 'http://localhost:9988/image/system/eae8d8a4-8fd1-4c97-9f29-6432d1699004.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:58:14');
INSERT INTO `mall_item_sale_class` VALUES ('293', '109243035', '锅底/丸子', '289', 'http://localhost:9988/image/system/a98d3885-75e2-4efa-b125-c8834a144037.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:58:40');
INSERT INTO `mall_item_sale_class` VALUES ('294', '109243036', '时蔬净菜', '289', 'http://localhost:9988/image/system/c73e86a0-0b9f-46ce-a923-e538d08be0b3.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:58:59');
INSERT INTO `mall_item_sale_class` VALUES ('296', '109243029', '酒水饮料', '0', 'http://localhost:9988/image/system/795c533f-988d-4dbb-af72-6838a7823174.png', '1', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 22:43:07');
INSERT INTO `mall_item_sale_class` VALUES ('297', '109312000', '饮用水', '296', 'http://localhost:9988/image/system/828b0f29-2491-4eaa-b355-d1318fd3a723.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:59:20');
INSERT INTO `mall_item_sale_class` VALUES ('298', '109312001', '碳酸/运动', '296', 'http://localhost:9988/image/system/4e047cc7-8cfa-4ab0-be44-b62c760db6c5.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:59:34');
INSERT INTO `mall_item_sale_class` VALUES ('299', '109312002', '茶饮料', '296', 'http://localhost:9988/image/system/eb5c28bc-2316-48a1-ba93-3ba370f304dd.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:59:43');
INSERT INTO `mall_item_sale_class` VALUES ('300', '109312003', '果汁饮料', '296', 'http://localhost:9988/image/system/7b150313-cfe0-4f21-b536-c471a9351159.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 09:59:50');
INSERT INTO `mall_item_sale_class` VALUES ('301', '109313000', '椰汁/豆奶', '296', 'http://localhost:9988/image/system/945c5297-d487-4572-98e9-46bfb09bdf80.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 10:00:02');
INSERT INTO `mall_item_sale_class` VALUES ('302', '109313003', '葡萄酒', '296', 'http://localhost:9988/image/system/5a46dd66-0afd-47e0-89ae-dd19693dffe0.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 10:00:09');
INSERT INTO `mall_item_sale_class` VALUES ('303', '19999999', '粮油调味', '0', 'http://localhost:9988/image/system/5593cc0c-6cf9-4020-a81a-27a163cfa0e5.png', '1', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 22:42:45');
INSERT INTO `mall_item_sale_class` VALUES ('304', '19999999001', '大米', '303', 'http://localhost:9988/image/system/c5e5ab75-22af-4034-bf22-42ea8e03e1f6.png', '0', 'admin', '2024-01-27 12:59:22', 'admin', '2024-07-16 10:00:26');
INSERT INTO `mall_item_sale_class` VALUES ('305', '20000000', '冻品面点', '0', 'http://localhost:9988/image/system/44caa756-d6d4-4187-9be6-b5c298d6ca3e.png', '1', 'admin', '2024-07-16 10:01:19', 'admin', '2024-07-16 22:42:33');
INSERT INTO `mall_item_sale_class` VALUES ('306', '2000000001', '冰淇淋', '305', null, '1', 'admin', '2024-07-16 10:01:32', null, null);
INSERT INTO `mall_item_sale_class` VALUES ('307', '2000000002', '包子/馒头', '305', null, '1', 'admin', '2024-07-16 10:01:45', null, null);
INSERT INTO `mall_item_sale_class` VALUES ('308', '2000000003', '饺子/馄饨', '305', null, '1', 'admin', '2024-07-16 10:01:55', null, null);

-- ----------------------------
-- Table structure for mall_item_sale_sku
-- ----------------------------
DROP TABLE IF EXISTS `mall_item_sale_sku`;
CREATE TABLE `mall_item_sale_sku` (
  `item_sale_sku_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_code` varchar(255) DEFAULT NULL,
  `item_name` varchar(1024) DEFAULT NULL,
  `unit_quantity` int(11) DEFAULT NULL,
  `item_unit_name` varchar(25) DEFAULT NULL,
  `item_sale_id` int(11) DEFAULT NULL,
  `item_sale_name` varchar(1024) DEFAULT NULL,
  `sku_code` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `source_price` decimal(10,2) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `spec` varchar(1024) DEFAULT NULL,
  `spec_map_json` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`item_sale_sku_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5450 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_item_unit
-- ----------------------------
DROP TABLE IF EXISTS `mall_item_unit`;
CREATE TABLE `mall_item_unit` (
  `item_unit_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `item_unit_name` varchar(25) NOT NULL COMMENT '单位名称',
  `created_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_name` varchar(255) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`item_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mall_item_unit
-- ----------------------------
INSERT INTO `mall_item_unit` VALUES ('1', 'g', 'admin', '2024-07-12 18:26:34', 'admin', '2024-07-12 18:28:47');
INSERT INTO `mall_item_unit` VALUES ('2', 'kg', 'admin', '2024-07-12 18:28:52', null, null);
INSERT INTO `mall_item_unit` VALUES ('3', '克', 'admin', '2024-07-12 18:29:00', null, null);
INSERT INTO `mall_item_unit` VALUES ('4', '千克', 'admin', '2024-07-12 18:29:03', null, null);
INSERT INTO `mall_item_unit` VALUES ('5', '盒', 'admin', '2024-07-12 18:29:08', null, null);
INSERT INTO `mall_item_unit` VALUES ('6', '支', 'admin', '2024-07-12 18:29:15', null, null);
INSERT INTO `mall_item_unit` VALUES ('7', '支', 'admin', '2024-07-12 18:29:20', null, null);
INSERT INTO `mall_item_unit` VALUES ('8', '箱', 'admin', '2024-07-12 18:29:22', null, null);
INSERT INTO `mall_item_unit` VALUES ('9', '瓶', 'admin', '2024-07-12 18:29:50', null, null);
INSERT INTO `mall_item_unit` VALUES ('10', '包', 'admin', '2024-07-12 18:29:59', null, null);
INSERT INTO `mall_item_unit` VALUES ('11', '斤', 'admin', '2024-07-12 18:30:36', null, null);
INSERT INTO `mall_item_unit` VALUES ('12', '公斤', 'admin', '2024-07-12 18:30:41', null, null);
INSERT INTO `mall_item_unit` VALUES ('14', '件', 'admin', '2024-07-13 10:32:32', null, null);
INSERT INTO `mall_item_unit` VALUES ('15', '袋', 'admin', '2024-07-13 10:49:54', null, null);
INSERT INTO `mall_item_unit` VALUES ('16', '杯', 'admin', '2024-07-13 10:51:09', null, null);

-- ----------------------------
-- Table structure for mall_member
-- ----------------------------
DROP TABLE IF EXISTS `mall_member`;
CREATE TABLE `mall_member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_member
-- ----------------------------
INSERT INTO `mall_member` VALUES ('5', '15622600001', '张三丰', 'http://localhost:9988/image/user/a093680d-2dfe-4f4b-b1ea-d23ba24f499c.png', '15622600001', '男', '2024-01-27', '北京市 北京市 石景山区', 'admin', '2024-01-29 17:09:08', null, null);
INSERT INTO `mall_member` VALUES ('6', '15600000000', '格温', 'http://localhost:9988/image/user/cd78ee6c-915b-4652-9a3e-efba7e6d2543.png', '15600000000', '男', '2024-03-30', '北京市 北京市 东城区', null, null, null, null);
INSERT INTO `mall_member` VALUES ('7', '15000000000', '15000000000', 'http://localhost:9988/image/user/7176a5d2-f7ee-45ac-8901-7ebc8abd135e.png', '15000000000', '男', '2024-06-20', '北京市 北京市 东城区', null, null, null, null);

-- ----------------------------
-- Table structure for mall_member_address
-- ----------------------------
DROP TABLE IF EXISTS `mall_member_address`;
CREATE TABLE `mall_member_address` (
  `member_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `province_code` varchar(255) DEFAULT NULL,
  `city_code` varchar(255) DEFAULT NULL,
  `county_code` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `default_address` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`member_address_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_member_cart
-- ----------------------------
DROP TABLE IF EXISTS `mall_member_cart`;
CREATE TABLE `mall_member_cart` (
  `member_cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(255) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `item_sale_channel_id` int(11) DEFAULT NULL,
  `item_sale_id` int(11) DEFAULT NULL,
  `item_sale_sku_id` int(11) DEFAULT NULL,
  `item_sale_name` varchar(255) DEFAULT NULL,
  `attrs_text` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `add_price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`member_cart_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_member_cart
-- ----------------------------

-- ----------------------------
-- Table structure for mall_member_coupon
-- ----------------------------
DROP TABLE IF EXISTS `mall_member_coupon`;
CREATE TABLE `mall_member_coupon` (
  `member_coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `coupon_no` varchar(255) DEFAULT NULL,
  `coupon_status` int(1) DEFAULT NULL,
  `start_date` date DEFAULT NULL COMMENT '开始日期',
  `end_date` date DEFAULT NULL COMMENT '结束日期',
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_coupon_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_sale_order
-- ----------------------------
DROP TABLE IF EXISTS `mall_sale_order`;
CREATE TABLE `mall_sale_order` (
  `sale_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(20) DEFAULT NULL,
  `sys_branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `pay_amount` decimal(10,2) DEFAULT NULL,
  `discount_amount` decimal(10,2) DEFAULT NULL,
  `post_fee` decimal(10,2) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_tel` varchar(255) DEFAULT NULL,
  `receiver_address` varchar(255) DEFAULT NULL,
  `sale_channel` int(11) DEFAULT NULL,
  `order_status` int(2) DEFAULT NULL,
  `pay_status` int(1) DEFAULT NULL,
  `pay_channel` int(1) DEFAULT NULL,
  `pay_type` int(1) DEFAULT NULL,
  `pay_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `total_quantity` int(11) DEFAULT NULL,
  `member_remark` varchar(255) DEFAULT NULL,
  `cancel_reason` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sale_order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_sale_order
-- ----------------------------

-- ----------------------------
-- Table structure for mall_sale_order_line
-- ----------------------------
DROP TABLE IF EXISTS `mall_sale_order_line`;
CREATE TABLE `mall_sale_order_line` (
  `sale_order_line_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_order_id` int(11) DEFAULT NULL,
  `item_sale_channel_id` int(11) DEFAULT NULL,
  `item_sale_id` int(11) DEFAULT NULL,
  `item_sale_sku_id` int(11) DEFAULT NULL,
  `sku_code` varchar(255) DEFAULT NULL,
  `item_sale_name` varchar(255) DEFAULT NULL,
  `attrs_text` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `quantity` decimal(10,0) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `pay_price` decimal(10,2) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `pay_amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`sale_order_line_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_sale_order_line
-- ----------------------------

-- ----------------------------
-- Table structure for mall_spec
-- ----------------------------
DROP TABLE IF EXISTS `mall_spec`;
CREATE TABLE `mall_spec` (
  `spec_id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_name` varchar(255) DEFAULT NULL,
  `sku_str` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`spec_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_spec
-- ----------------------------
INSERT INTO `mall_spec` VALUES ('5', '规格', '300g|500g', '无', 'admin', '2023-12-23 17:12:14', 'admin', '2024-07-16 11:41:35');
INSERT INTO `mall_spec` VALUES ('6', '甜度', '标准糖|少糖|不添加糖', '无', 'admin', '2023-12-23 17:12:51', null, '2023-12-25 15:19:15');
INSERT INTO `mall_spec` VALUES ('7', '内存', '256g|512g', null, 'admin', '2024-01-06 13:51:23', null, null);
INSERT INTO `mall_spec` VALUES ('8', '颜色', '蓝色|白色', null, 'admin', '2024-01-06 14:04:50', null, null);
INSERT INTO `mall_spec` VALUES ('9', '套装', '2L可乐|2L雪碧', null, 'admin', '2024-01-07 01:27:20', null, null);
INSERT INTO `mall_spec` VALUES ('11', 'aaa', '11', 'ss', '李白', '2024-02-26 18:17:09', '李白', '2024-02-26 18:17:38');
INSERT INTO `mall_spec` VALUES ('12', '规格', '300g|400g|500g', null, 'admin', '2024-07-13 14:07:28', null, null);
INSERT INTO `mall_spec` VALUES ('14', '温度', '冷藏|常温', null, 'admin', '2024-07-16 11:34:22', null, null);

-- ----------------------------
-- Table structure for mall_spec_value
-- ----------------------------
DROP TABLE IF EXISTS `mall_spec_value`;
CREATE TABLE `mall_spec_value` (
  `spec_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_value_name` varchar(255) DEFAULT NULL,
  `spec_id` int(11) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`spec_value_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_spec_value
-- ----------------------------
INSERT INTO `mall_spec_value` VALUES ('25', '标准糖', '6', 'admin', '2023-12-25 15:19:15', null, null);
INSERT INTO `mall_spec_value` VALUES ('26', '少糖', '6', 'admin', '2023-12-25 15:19:15', null, null);
INSERT INTO `mall_spec_value` VALUES ('27', '不添加糖', '6', 'admin', '2023-12-25 15:19:15', null, null);
INSERT INTO `mall_spec_value` VALUES ('28', '256g', '7', 'admin', '2024-01-06 13:51:23', null, null);
INSERT INTO `mall_spec_value` VALUES ('29', '512g', '7', 'admin', '2024-01-06 13:51:23', null, null);
INSERT INTO `mall_spec_value` VALUES ('30', '蓝色', '8', 'admin', '2024-01-06 14:04:50', null, null);
INSERT INTO `mall_spec_value` VALUES ('31', '白色', '8', 'admin', '2024-01-06 14:04:50', null, null);
INSERT INTO `mall_spec_value` VALUES ('32', '2L可乐', '9', 'admin', '2024-01-07 01:27:20', null, null);
INSERT INTO `mall_spec_value` VALUES ('33', '2L雪碧', '9', 'admin', '2024-01-07 01:27:20', null, null);
INSERT INTO `mall_spec_value` VALUES ('43', '11', '11', '李白', '2024-02-26 18:17:38', null, null);
INSERT INTO `mall_spec_value` VALUES ('44', '300g', '12', 'admin', '2024-07-13 14:07:28', null, null);
INSERT INTO `mall_spec_value` VALUES ('45', '400g', '12', 'admin', '2024-07-13 14:07:28', null, null);
INSERT INTO `mall_spec_value` VALUES ('46', '500g', '12', 'admin', '2024-07-13 14:07:28', null, null);
INSERT INTO `mall_spec_value` VALUES ('53', '冷藏', '14', 'admin', '2024-07-16 11:34:22', null, null);
INSERT INTO `mall_spec_value` VALUES ('54', '常温', '14', 'admin', '2024-07-16 11:34:22', null, null);
INSERT INTO `mall_spec_value` VALUES ('55', '300g', '5', 'admin', '2024-07-16 11:41:35', null, null);
INSERT INTO `mall_spec_value` VALUES ('56', '500g', '5', 'admin', '2024-07-16 11:41:35', null, null);

-- ----------------------------
-- Table structure for mall_subject
-- ----------------------------
DROP TABLE IF EXISTS `mall_subject`;
CREATE TABLE `mall_subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `subject_description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`subject_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_subject_item_sale
-- ----------------------------
DROP TABLE IF EXISTS `mall_subject_item_sale`;
CREATE TABLE `mall_subject_item_sale` (
  `subject_item_sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) DEFAULT NULL,
  `item_sale_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`subject_item_sale_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for mall_tag
-- ----------------------------
DROP TABLE IF EXISTS `mall_tag`;
CREATE TABLE `mall_tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_type` int(11) DEFAULT NULL,
  `tag_name` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tag_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mall_tag
-- ----------------------------
INSERT INTO `mall_tag` VALUES ('1', '1', '新品', 'admin', '2023-12-26 16:31:27', null, '2023-12-26 16:40:46');
INSERT INTO `mall_tag` VALUES ('3', '2', '常客', 'admin', '2023-12-26 18:20:15', null, null);
INSERT INTO `mall_tag` VALUES ('4', '1', '热销', 'admin', '2023-12-30 12:52:39', null, null);
INSERT INTO `mall_tag` VALUES ('5', '1', '疯狂星期四', 'admin', '2023-12-30 12:52:50', null, null);

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app` (
  `sys_app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(50) DEFAULT NULL,
  `app_secret` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sys_app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_app
-- ----------------------------
INSERT INTO `sys_app` VALUES ('2', '1000', 'ncqa9b6nzaemgvy8wg2m2euu', '测试应用', 'admin', '2024-07-08 10:10:15', 'admin', '2024-07-16 09:26:07');
INSERT INTO `sys_app` VALUES ('3', '99', 'ttvt8woqpaxpf82ofjckdazf', '测试', 'admin', '2024-07-09 10:10:18', 'admin', '2024-07-16 09:26:04');

-- ----------------------------
-- Table structure for sys_branch
-- ----------------------------
DROP TABLE IF EXISTS `sys_branch`;
CREATE TABLE `sys_branch` (
  `sys_branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_code` varchar(25) DEFAULT NULL,
  `branch_name` varchar(50) DEFAULT NULL,
  `branch_tel` varchar(25) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `branch_address` varchar(255) DEFAULT NULL,
  `post_code` varchar(50) DEFAULT NULL,
  `branch_status` tinyint(1) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sys_branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_branch
-- ----------------------------
INSERT INTO `sys_branch` VALUES ('6', '201001', '广州富力半岛店', '15622000001', '河北省', '秦皇岛市', '抚宁区', '增槎路富力半岛1栋1号', '100', '1', 'admin', '2024-06-20 18:30:32', 'admin', '2024-06-20 18:30:32');
INSERT INTO `sys_branch` VALUES ('7', '201002', '广州保利海岸店', '15622000002', '山西省', '阳泉市', '平定县', '白云彩滨北路保利海岸1单元2号', '150', '1', 'admin', '2024-06-20 18:30:36', 'admin', '2024-06-20 18:30:36');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `sys_config_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `config_options` varchar(255) DEFAULT NULL,
  `config_key` varchar(255) DEFAULT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sys_config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '登录验证方式', 'radio', '[{\"label\":\"随机验证码\",\"value\":\"captcha\"},{\"label\":\"滑块验证\",\"value\":\"slider\"}]', 'VERIFY_METHOD', 'captcha');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_code` varchar(255) DEFAULT NULL,
  `dict_name` varchar(255) DEFAULT NULL,
  `dict_type` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `dict_sort` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', 'coupon_type', '券类型', '1', '优惠券类型', '0', 'admin', '2024-01-01 19:02:07', null, '2024-02-26 10:05:51');
INSERT INTO `sys_dict` VALUES ('2', 'sale_channel', '销售渠道', '1', '销售渠道', '0', 'admin', '2024-01-01 20:43:23', null, null);
INSERT INTO `sys_dict` VALUES ('3', 'branch_status', '门店状态', '1', '门店状态', '0', 'admin', '2024-01-02 11:40:11', null, null);
INSERT INTO `sys_dict` VALUES ('4', 'branch_type', '门店类型', '1', '门店类型', '0', 'admin', '2024-01-02 11:40:43', null, null);
INSERT INTO `sys_dict` VALUES ('5', 'bill_status', '单据状态', '1', '单据状态', '0', 'admin', '2024-01-02 11:43:28', null, '2024-02-02 11:51:30');
INSERT INTO `sys_dict` VALUES ('7', 'coupon_status', '券状态', '1', '券状态', '0', 'admin', '2024-01-02 11:45:17', null, '2024-01-02 11:48:18');
INSERT INTO `sys_dict` VALUES ('8', 'bill_type', '单据类型', '1', '单据类型', '0', 'admin', '2024-01-02 11:47:20', null, null);
INSERT INTO `sys_dict` VALUES ('9', 'tag_type', '标签类型', '1', '标签类型', '0', 'admin', '2024-01-02 11:49:14', null, null);
INSERT INTO `sys_dict` VALUES ('10', 'dict_type', '字典类型', '1', '字典类型', '0', 'admin', '2024-01-02 11:50:02', null, null);
INSERT INTO `sys_dict` VALUES ('11', 'order_status', '订单状态', '1', '订单状态', '0', 'admin', '2024-02-02 11:52:33', null, null);

-- ----------------------------
-- Table structure for sys_dict_value
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_value`;
CREATE TABLE `sys_dict_value` (
  `dict_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `dict_sort` varchar(255) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dict_value_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dict_value
-- ----------------------------
INSERT INTO `sys_dict_value` VALUES ('1', '1', '1', '满减', null, '0', 'admin', '2024-01-01 20:59:57', null, '2024-01-01 20:59:57');
INSERT INTO `sys_dict_value` VALUES ('2', '1', '2', '折扣', null, '0', 'admin', '2024-01-01 20:59:59', null, '2024-01-01 20:59:59');
INSERT INTO `sys_dict_value` VALUES ('4', '2', '1', '门店收银机', null, '0', 'admin', '2024-01-01 21:30:59', null, null);
INSERT INTO `sys_dict_value` VALUES ('5', '2', '2', '小程序', null, '0', 'admin', '2024-01-01 21:32:13', null, '2024-01-01 21:32:13');
INSERT INTO `sys_dict_value` VALUES ('6', '2', '3', '美团', null, '0', 'admin', '2024-01-01 21:32:33', null, null);
INSERT INTO `sys_dict_value` VALUES ('7', '2', '4', '饿了么', null, '0', 'admin', '2024-01-01 21:32:40', null, null);
INSERT INTO `sys_dict_value` VALUES ('8', '3', '1', '营业', null, '0', 'admin', '2024-01-02 11:40:22', null, null);
INSERT INTO `sys_dict_value` VALUES ('9', '3', '2', '闭店', null, '0', 'admin', '2024-01-02 11:40:27', null, null);
INSERT INTO `sys_dict_value` VALUES ('10', '4', '1', '门店', null, '0', 'admin', '2024-01-02 11:40:56', null, null);
INSERT INTO `sys_dict_value` VALUES ('11', '4', '2', '仓库', null, '0', 'admin', '2024-01-02 11:40:58', null, null);
INSERT INTO `sys_dict_value` VALUES ('12', '5', '1', '制单', null, '0', 'admin', '2024-01-02 11:43:36', null, null);
INSERT INTO `sys_dict_value` VALUES ('13', '5', '2', '审核', null, '0', 'admin', '2024-01-02 11:43:40', null, null);
INSERT INTO `sys_dict_value` VALUES ('16', '7', '1', '未使用', null, '0', 'admin', '2024-01-02 11:45:26', null, '2024-01-02 11:46:04');
INSERT INTO `sys_dict_value` VALUES ('17', '7', '2', '已使用', null, '0', 'admin', '2024-01-02 11:45:36', null, '2024-01-02 11:46:09');
INSERT INTO `sys_dict_value` VALUES ('18', '8', '1', '销售出库单', null, '0', 'admin', '2024-01-02 11:47:37', null, '2024-02-26 10:19:41');
INSERT INTO `sys_dict_value` VALUES ('19', '8', '2', '销售退货单', null, '0', 'admin', '2024-01-02 11:47:43', null, '2024-02-26 10:19:36');
INSERT INTO `sys_dict_value` VALUES ('20', '8', '3', '采购入库单', null, '0', 'admin', '2024-01-02 11:47:51', null, '2024-02-26 10:19:46');
INSERT INTO `sys_dict_value` VALUES ('23', '9', '1', '商品', null, '0', 'admin', '2024-01-02 11:49:21', null, null);
INSERT INTO `sys_dict_value` VALUES ('24', '9', '2', '用户', null, '0', 'admin', '2024-01-02 11:49:25', null, null);
INSERT INTO `sys_dict_value` VALUES ('25', '10', '1', '系统字典', null, '0', 'admin', '2024-01-02 11:50:10', null, '2024-01-05 11:49:46');
INSERT INTO `sys_dict_value` VALUES ('28', '10', '2', '业务字典', null, '0', 'admin', '2024-01-05 12:35:54', null, null);
INSERT INTO `sys_dict_value` VALUES ('29', '11', '1', '待付款', null, '0', 'admin', '2024-02-02 11:52:54', null, null);
INSERT INTO `sys_dict_value` VALUES ('30', '11', '2', '待发货', null, '0', 'admin', '2024-02-02 11:53:01', null, null);
INSERT INTO `sys_dict_value` VALUES ('31', '11', '3', '待收货', null, '0', 'admin', '2024-02-02 11:53:06', null, null);
INSERT INTO `sys_dict_value` VALUES ('32', '11', '4', '待评价', null, '0', 'admin', '2024-02-02 11:53:14', null, null);
INSERT INTO `sys_dict_value` VALUES ('33', '11', '5', '已完成', null, '0', 'admin', '2024-02-02 11:53:22', null, null);
INSERT INTO `sys_dict_value` VALUES ('35', '8', '4', '采购出库单', null, '0', 'admin', '2024-02-03 16:25:11', null, '2024-02-26 10:19:55');
INSERT INTO `sys_dict_value` VALUES ('36', '8', '5', '盘点单', null, '0', 'admin', '2024-02-03 16:25:18', null, '2024-02-26 10:20:02');
INSERT INTO `sys_dict_value` VALUES ('37', '11', '6', '已取消', null, '0', 'admin', '2024-02-26 10:12:17', null, null);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `sys_job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_key` varchar(255) DEFAULT NULL,
  `job_group` varchar(255) DEFAULT NULL,
  `schedule_type` int(1) DEFAULT NULL,
  `cron` varchar(30) DEFAULT NULL,
  `bean` varchar(255) DEFAULT NULL,
  `start_now` tinyint(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `params` varchar(1024) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sys_job_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('7', 'coupon', 'couponGroup', '1', '0/2 * * * * ? *', 'couponJob', '0', '2', '999', '456');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `sys_job_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_job_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT NULL,
  `error_msg` text,
  `execute_spend` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`sys_job_log_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `sys_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `request_param` text,
  `request_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `request_spend` int(10) DEFAULT NULL,
  `request_ip` varchar(255) DEFAULT NULL,
  `error_msg` text,
  PRIMARY KEY (`sys_log_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=397 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `sys_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) DEFAULT NULL,
  `router_name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `router_path` varchar(255) DEFAULT NULL,
  `router_component` varchar(255) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL,
  `permission_code` varchar(255) DEFAULT NULL,
  `menu_sort` int(11) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sys_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '首页', 'index', '0', '/home/index', '/views/index.vue', 'House', 'index', '99', 'admin', '2024-07-15 18:20:00', null, '2024-07-15 18:21:39');
INSERT INTO `sys_menu` VALUES ('2', '系统', null, '0', null, null, 'Monitor', null, '3', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('3', '基础资料', null, '2', null, null, 'Files', null, '99', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('4', '菜单管理', 'menu', '3', '/home/menu', '/views/system/base/menu/index.vue', null, 'menu', '99', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('5', '角色管理', 'role', '3', '/home/role', '/views/system/base/role/index.vue', null, 'role', '3', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('6', '用户管理', 'user', '3', '/home/user', '/views/system/base/user/index.vue', null, 'user', '2', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('7', '应用管理', 'app', '3', '/home/app', '/views/system/base/app/index.vue', null, 'app', '1', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('8', '开发配置', null, '2', null, null, 'Setting', null, '98', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('9', '字典管理', 'dict', '8', '/home/dict', '/views/system/setting/dict/index.vue', null, 'dict', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('10', '操作日志', 'log', '8', '/home/log', '/views/system/setting/log/index.vue', null, 'log', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('11', '定时任务', 'job', '8', '/home/job', '/views/system/setting/job/index.vue', null, 'job', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('12', '系统配置', 'config', '8', '/home/config', '/views/system/setting/config/index.vue', null, 'config', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('13', '数据库监控', 'sql', '8', '/home/sql', '/views/system/setting/sql/index.vue', null, 'sql', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('14', '接口文档', 'doc', '8', '/home/doc', '/views/system/setting/doc/index.vue', null, 'doc', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('15', '应用监控', 'health', '8', '/home/health', '/views/system/setting/health/index.vue', null, 'health', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('16', '零售', null, '0', null, null, 'ShoppingBag', null, '2', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('17', '商品中心', null, '16', null, null, 'Goods', null, '99', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('18', '营销分类', 'itemSaleClass', '17', '/home/itemsaleclass', '/views/mall/items/itemSaleClass/index.vue', null, 'itemSaleClass', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('19', '规格管理', 'spec', '17', '/home/spec', '/views/mall/items/spec/index.vue', null, 'spec', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('20', '销售商品', 'itemSale', '17', '/home/itemsale', '/views/mall/items/itemSale/index.vue', null, 'itemSale', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('21', '订单中心', null, '16', null, null, 'Tickets', null, '98', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('22', '销售订单', 'saleOrder', '21', '/home/saleorder', '/views/mall/order/saleOrder/index.vue', null, 'saleOrder', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('23', '会员中心', null, '16', null, null, 'User', null, '97', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('24', '会员管理', 'member', '23', '/home/member', '/views/mall/member/member/index.vue', null, 'member', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('25', '营销中心', null, '16', null, null, 'DataAnalysis', null, '96', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('26', '标签管理', 'tag', '25', '/home/tag', '/views/mall/marketing/tag/index.vue', null, 'tag', '99', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('27', '优惠券管理', 'coupon', '25', '/home/coupon', '/views/mall/marketing/coupon/index.vue', null, 'coupon', '96', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('28', '专题管理', 'subject', '25', '/home/subject', '/views/mall/marketing/subject/index.vue', null, 'subject', '98', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('29', '广告管理', 'banner', '25', '/home/banner', '/views/mall/marketing/banner/index.vue', null, 'banner', '97', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('30', 'ERP', null, '0', null, null, 'OfficeBuilding', null, '1', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('31', '库存中心', null, '30', null, null, 'Box', null, '99', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('32', '库存查询', 'stock', '31', '/home/stock', '/views/erp/stock/stock/index.vue', null, 'stock', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('33', '盘点订单', 'inventoryOrder', '31', '/home/inventoryorder', '/views/erp/stock/inventoryOrder/index.vue', null, 'inventoryOrder', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('34', '采购中心', null, '30', null, null, 'ShoppingTrolley', null, '98', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('35', '采购入库单', 'purchaseInOrder', '34', '/home/purchaseinorder', '/views/erp/purchase/purchaseInOrder/index.vue', null, 'purchaseInOrder', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('36', '采购出库单', 'purchaseOutOrder', '34', '/home/purchaseoutorder', '/views/erp/purchase/purchaseOutOrder/index.vue', null, 'purchaseOutOrder', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('37', '测试模块', '', '0', '', '', null, '', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('38', '测试管理', 'testTable', '37', '/home/testTable', '/views/test/table/index.vue', null, 'test', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('42', '岗位管理', 'post', '3', '/home/post', '/views/system/base/post/index.vue', null, 'post', '3', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('43', '定向发券', 'couponDirect', '25', '/home/couponDirect', '/views/mall/marketing/couponDirect/index.vue', null, 'couponDirect', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('44', '门店管理', 'branch', '3', '/home/branch', '/views/system/base/branch/index.vue', null, 'branch', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('45', '渠道商品', 'itemSaleChannel', '17', '/home/itemSaleChannel', '/views/mall/items/itemSaleChannel/index.vue', null, 'itemSaleChannel', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('46', '代码生成', 'gen', '8', '/home/gen', '/views/system/setting/gen/index.vue', null, 'gen', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('47', '基础单位', 'itemUnit', '17', '/home/itemUnit', '/views/mall/items/itemUnit/index.vue', null, 'itemUnit', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('48', '基础商品', 'item', '17', '/home/item', '/views/mall/items/item/index.vue', null, 'item', '0', 'admin', '2024-07-15 18:21:07', null, '2024-07-15 18:21:15');
INSERT INTO `sys_menu` VALUES ('49', '缓存监控', 'cache', '8', '/home/cache', '/views/system/setting/cache/index.vue', NULL, 'cache', '0', 'admin', '2024-08-07 10:50:18', 'admin', '2024-08-07 10:50:29');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `sys_post_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_post_code` varchar(50) DEFAULT NULL,
  `sys_post_name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `post_sort` int(10) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sys_post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'java', 'Java开发工程师', '测试', '999', 'admin', '2024-03-13 11:01:14', 'admin', '2024-03-14 11:11:33');
INSERT INTO `sys_post` VALUES ('2', 'android', 'android开发工程师', '测试2', '998', 'admin', '2024-03-13 11:02:11', 'admin', '2024-05-31 17:01:34');
INSERT INTO `sys_post` VALUES ('3', 'go', 'go后端工程师', '测试3', '997', 'admin', '2024-03-13 11:33:58', null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `sys_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `role_type` int(1) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sys_role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('3', '超级管理员', '1', 'admin', '2023-12-23 09:53:07', 'admin', '2024-07-16 09:23:49');
INSERT INTO `sys_role` VALUES ('4', '运营', '2', 'admin', '2023-12-23 09:53:43', 'admin', '2024-07-16 09:24:43');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `sys_role_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_role_id` int(11) DEFAULT NULL,
  `sys_menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sys_role_menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=974 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('911', '3', '1');
INSERT INTO `sys_role_menu` VALUES ('912', '3', '2');
INSERT INTO `sys_role_menu` VALUES ('913', '3', '3');
INSERT INTO `sys_role_menu` VALUES ('914', '3', '4');
INSERT INTO `sys_role_menu` VALUES ('915', '3', '5');
INSERT INTO `sys_role_menu` VALUES ('916', '3', '42');
INSERT INTO `sys_role_menu` VALUES ('917', '3', '6');
INSERT INTO `sys_role_menu` VALUES ('918', '3', '7');
INSERT INTO `sys_role_menu` VALUES ('919', '3', '44');
INSERT INTO `sys_role_menu` VALUES ('920', '3', '8');
INSERT INTO `sys_role_menu` VALUES ('921', '3', '9');
INSERT INTO `sys_role_menu` VALUES ('922', '3', '10');
INSERT INTO `sys_role_menu` VALUES ('923', '3', '11');
INSERT INTO `sys_role_menu` VALUES ('924', '3', '12');
INSERT INTO `sys_role_menu` VALUES ('925', '3', '13');
INSERT INTO `sys_role_menu` VALUES ('926', '3', '14');
INSERT INTO `sys_role_menu` VALUES ('927', '3', '15');
INSERT INTO `sys_role_menu` VALUES ('928', '3', '46');
INSERT INTO `sys_role_menu` VALUES ('929', '3', '16');
INSERT INTO `sys_role_menu` VALUES ('930', '3', '17');
INSERT INTO `sys_role_menu` VALUES ('931', '3', '18');
INSERT INTO `sys_role_menu` VALUES ('932', '3', '19');
INSERT INTO `sys_role_menu` VALUES ('933', '3', '20');
INSERT INTO `sys_role_menu` VALUES ('934', '3', '45');
INSERT INTO `sys_role_menu` VALUES ('935', '3', '47');
INSERT INTO `sys_role_menu` VALUES ('936', '3', '48');
INSERT INTO `sys_role_menu` VALUES ('937', '3', '21');
INSERT INTO `sys_role_menu` VALUES ('938', '3', '22');
INSERT INTO `sys_role_menu` VALUES ('939', '3', '23');
INSERT INTO `sys_role_menu` VALUES ('940', '3', '24');
INSERT INTO `sys_role_menu` VALUES ('941', '3', '25');
INSERT INTO `sys_role_menu` VALUES ('942', '3', '26');
INSERT INTO `sys_role_menu` VALUES ('943', '3', '28');
INSERT INTO `sys_role_menu` VALUES ('944', '3', '29');
INSERT INTO `sys_role_menu` VALUES ('945', '3', '27');
INSERT INTO `sys_role_menu` VALUES ('946', '3', '43');
INSERT INTO `sys_role_menu` VALUES ('947', '3', '30');
INSERT INTO `sys_role_menu` VALUES ('948', '3', '31');
INSERT INTO `sys_role_menu` VALUES ('949', '3', '32');
INSERT INTO `sys_role_menu` VALUES ('950', '3', '33');
INSERT INTO `sys_role_menu` VALUES ('951', '3', '34');
INSERT INTO `sys_role_menu` VALUES ('952', '3', '35');
INSERT INTO `sys_role_menu` VALUES ('953', '3', '36');
INSERT INTO `sys_role_menu` VALUES ('954', '3', '37');
INSERT INTO `sys_role_menu` VALUES ('955', '3', '38');
INSERT INTO `sys_role_menu` VALUES ('956', '4', '16');
INSERT INTO `sys_role_menu` VALUES ('957', '4', '17');
INSERT INTO `sys_role_menu` VALUES ('958', '4', '18');
INSERT INTO `sys_role_menu` VALUES ('959', '4', '19');
INSERT INTO `sys_role_menu` VALUES ('960', '4', '20');
INSERT INTO `sys_role_menu` VALUES ('961', '4', '45');
INSERT INTO `sys_role_menu` VALUES ('962', '4', '47');
INSERT INTO `sys_role_menu` VALUES ('963', '4', '48');
INSERT INTO `sys_role_menu` VALUES ('964', '4', '21');
INSERT INTO `sys_role_menu` VALUES ('965', '4', '22');
INSERT INTO `sys_role_menu` VALUES ('966', '4', '23');
INSERT INTO `sys_role_menu` VALUES ('967', '4', '24');
INSERT INTO `sys_role_menu` VALUES ('968', '4', '25');
INSERT INTO `sys_role_menu` VALUES ('969', '4', '26');
INSERT INTO `sys_role_menu` VALUES ('970', '4', '28');
INSERT INTO `sys_role_menu` VALUES ('971', '4', '29');
INSERT INTO `sys_role_menu` VALUES ('972', '4', '27');
INSERT INTO `sys_role_menu` VALUES ('973', '4', '43');
INSERT INTO `sys_role_menu` VALUES ('974', '3', '49');

-- ----------------------------
-- Table structure for sys_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_table`;
CREATE TABLE `sys_table` (
  `table_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_name` varchar(255) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `table_comment` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `out_path` varchar(255) DEFAULT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_table
-- ----------------------------
INSERT INTO `sys_table` VALUES ('9', 'item_unit', 'CAMEL_CASE', 'ItemUnit', '基础单位', 'xiehuangbao', 'D:/tempCode', 'cn.code4java.springbok');

-- ----------------------------
-- Table structure for sys_table_column
-- ----------------------------
DROP TABLE IF EXISTS `sys_table_column`;
CREATE TABLE `sys_table_column` (
  `table_column_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) DEFAULT NULL,
  `column_name` varchar(255) DEFAULT NULL,
  `column_type` varchar(255) DEFAULT NULL,
  `column_length` int(5) DEFAULT NULL,
  `decimal_length` int(5) DEFAULT NULL,
  `field_name` varchar(255) DEFAULT NULL,
  `field_type` varchar(255) DEFAULT NULL,
  `column_comment` varchar(255) DEFAULT NULL,
  `primary_key` tinyint(1) DEFAULT NULL,
  `auto_increment` tinyint(1) DEFAULT NULL,
  `nullable` tinyint(1) DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`table_column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_table_column
-- ----------------------------
INSERT INTO `sys_table_column` VALUES ('36', '9', 'item_unit_id', 'int', '0', '0', 'itemUnitId', 'Integer', 'id', '1', '1', null, null);
INSERT INTO `sys_table_column` VALUES ('37', '9', 'item_unit_name', 'varchar', '25', '0', 'itemUnitName', 'String', '单位名称', null, null, null, null);
INSERT INTO `sys_table_column` VALUES ('38', '9', 'created_name', 'varchar', '0', '0', 'createdName', 'String', '创建人', null, null, '1', null);
INSERT INTO `sys_table_column` VALUES ('39', '9', 'created_time', 'datetime', '0', '0', 'createdTime', 'Date', '创建时间', null, null, '1', null);
INSERT INTO `sys_table_column` VALUES ('40', '9', 'updated_name', 'varchar', '0', '0', 'updatedName', 'String', '更新人', null, null, '1', null);
INSERT INTO `sys_table_column` VALUES ('41', '9', 'updated_time', 'datetime', '0', '0', 'updatedTime', 'Date', '更新时间', null, null, '1', null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `sys_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `sys_role_id` int(11) DEFAULT NULL,
  `sys_post_id` int(11) DEFAULT NULL,
  `created_name` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_name` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sys_user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'f22324b1c595901c9d6c86772af916cf', 'Springbok', '15622602256', '3', '3', 'admin', '2024-01-01 01:58:19', 'admin', '2024-03-13 14:28:15', '8db1bb79-aa88-4277-8fbf-ab836697a6b5');
INSERT INTO `sys_user` VALUES ('2', 'yanzhenqing', 'acab65141c8d0ce4f2616d22138c231a', '颜真卿', '15622602256', '4', '1', 'admin', '2023-12-22 11:16:34', 'admin', '2024-03-13 14:28:09', '2e9f7b62-0b64-4a61-b5cf-f2bd0c9a84b6');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `sys_user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_user_id` int(11) DEFAULT NULL,
  `sys_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sys_user_role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '3');

-- ----------------------------
-- Table structure for test_table
-- ----------------------------
DROP TABLE IF EXISTS `test_table`;
CREATE TABLE `test_table` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_col1` varchar(255) DEFAULT NULL,
  `test_col2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of test_table
-- ----------------------------
INSERT INTO `test_table` VALUES ('1', '测试数据1', '测试数据11');
INSERT INTO `test_table` VALUES ('2', '测试数据2', '测试数据22');
