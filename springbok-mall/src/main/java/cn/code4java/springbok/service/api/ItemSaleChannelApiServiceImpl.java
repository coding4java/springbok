package cn.code4java.springbok.service.api;

import cn.code4java.springbok.dto.ItemSaleChannelQueryDTO;
import cn.code4java.springbok.entity.ItemSale;
import cn.code4java.springbok.entity.ItemSaleChannel;
import cn.code4java.springbok.mapper.ItemSaleChannelMapper;
import cn.code4java.springbok.service.BaseServiceImpl;
import cn.code4java.springbok.vo.ItemSaleChannelSkuVO;
import cn.code4java.springbok.vo.ItemSaleChannelVO;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName ItemSaleChannelApiServiceImpl
 * @Description: 渠道商品API服务实现类
 * @Author xiehuangbao
 * @Date 2024/6/18
 * @Version V2.0.0
 **/
@Service
@AllArgsConstructor
public class ItemSaleChannelApiServiceImpl extends BaseServiceImpl<ItemSaleChannelMapper, ItemSaleChannel> implements ItemSaleChannelApiService {

    private ItemSaleChannelMapper itemSaleChannelMapper;

    @Override
    public Page<ItemSaleChannelVO> pageItemSaleChannel(ItemSaleChannelQueryDTO itemSaleChannelQueryDTO) {
        Page<ItemSaleChannelVO> itemSaleChannelPage = itemSaleChannelMapper.pageItemSaleChannel(getPage(), itemSaleChannelQueryDTO);
        itemSaleChannelPage.getRecords().stream().forEach(itemSaleChannelVO -> {
            itemSaleChannelVO.setMainImageList(ListUtil.toList(itemSaleChannelVO.getMainImage().split(",")));
        });
        return itemSaleChannelPage;
    }

    /**
     * 根据id查询渠道商品详情
     *
     * @param itemSaleChannelId
     * @return
     * @throws InterruptedException
     */
    @Override
    public ItemSaleChannelVO selectItemSaleChannelById(int itemSaleChannelId) {
        ItemSaleChannelVO itemSaleChannelVO = itemSaleChannelMapper.selectItemSaleChannelById(itemSaleChannelId);
        if (itemSaleChannelVO != null) {
            List<ItemSaleChannelSkuVO> itemSaleChannelSkus = itemSaleChannelMapper.selectItemSaleChannelSkuById(itemSaleChannelId);
            if (CollectionUtil.isNotEmpty(itemSaleChannelSkus)) {
                itemSaleChannelVO.setItemSaleSkuList(itemSaleChannelSkus);
            }
            itemSaleChannelVO.setSpecList(JSONUtil.toList(itemSaleChannelVO.getSpecJson(), ItemSale.Spec.class));
            itemSaleChannelVO.setPropertyList(JSONUtil.toList(itemSaleChannelVO.getPropertyJson(), ItemSale.Property.class));
            itemSaleChannelVO.setMainImageList(ListUtil.toList(itemSaleChannelVO.getMainImage().split(",")));
        }
        return itemSaleChannelVO;
    }
}
