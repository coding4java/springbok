package cn.code4java.springbok.service;

import cn.code4java.springbok.entity.Item;
import cn.code4java.springbok.mapper.ItemMapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

/**
 * @ClassName ItemServiceImpl
 * @Description: 服务实现类
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
@Service
public class ItemServiceImpl extends BaseServiceImpl<ItemMapper, Item> implements ItemService {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @Override
    public Page<Item> pageItem(Item params) {
        return this.page(getPage(), Wrappers.<Item>lambdaQuery().like(StringUtils.isNotBlank(params.getItemName()), Item::getItemName, params.getItemName()));
    }
}
