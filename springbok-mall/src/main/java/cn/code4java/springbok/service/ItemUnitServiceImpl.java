package cn.code4java.springbok.service;

import cn.code4java.springbok.entity.ItemUnit;
import cn.code4java.springbok.mapper.ItemUnitMapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

/**
 * @ClassName ItemUnitServiceImpl
 * @Description: 服务实现类
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
@Service
public class ItemUnitServiceImpl extends BaseServiceImpl<ItemUnitMapper, ItemUnit> implements ItemUnitService {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @Override
    public Page<ItemUnit> pageItemUnit(ItemUnit params) {
        return this.page(getPage(), Wrappers.<ItemUnit>lambdaQuery().like(StringUtils.isNotBlank(params.getItemUnitName()), ItemUnit::getItemUnitName, params.getItemUnitName()));
    }
}
