package cn.code4java.springbok.service.api;

import cn.code4java.springbok.dto.ItemSaleQueryDTO;
import cn.code4java.springbok.entity.ItemSale;
import cn.code4java.springbok.mapper.ItemSaleMapper;
import cn.code4java.springbok.mapper.ItemSaleSkuMapper;
import cn.code4java.springbok.service.BaseServiceImpl;
import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @ClassName ItemSaleApiServiceImpl
 * @Description: 商品API服务实现类
 * @Author fengwensheng
 * @Date 2023/11/22
 * @Version V1.0
 **/
@Service
@AllArgsConstructor
public class ItemSaleApiServiceImpl extends BaseServiceImpl<ItemSaleMapper, ItemSale> implements ItemSaleApiService {

    private ItemSaleMapper itemSaleMapper;
    private ItemSaleSkuMapper itemSaleSkuMapper;

    /**
     * 分页查询商品
     *
     * @param itemSaleQueryDTO
     * @return
     */
    @Override
    public Page<ItemSale> pageItemSale(ItemSaleQueryDTO itemSaleQueryDTO) {
        Page<ItemSale> itemSalePage = itemSaleMapper.pageItemSale(getPage(), itemSaleQueryDTO);
        itemSalePage.getRecords().stream().forEach(itemSale -> {
            itemSale.setMainImageList(ListUtil.toList(itemSale.getMainImage().split(",")));
        });
        return itemSalePage;
    }
}
