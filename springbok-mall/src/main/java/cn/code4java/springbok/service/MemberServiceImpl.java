package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.MemberQueryDTO;
import cn.code4java.springbok.entity.Member;
import cn.code4java.springbok.mapper.MemberMapper;
import cn.code4java.springbok.utils.excel.ExcelExportHandler;
import cn.code4java.springbok.utils.excel.ExcelImportHandler;
import cn.code4java.springbok.utils.excel.ExcelUtils;
import cn.code4java.springbok.vo.MemberVO;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @ClassName MemberServiceImpl
 * @Description: 会员服务实现类
 * @Author fengwensheng
 * @Date 2023/11/22
 * @Version V1.0
 **/
@Slf4j
@Service
@AllArgsConstructor
public class MemberServiceImpl extends BaseServiceImpl<MemberMapper, Member> implements MemberService {

    private MemberMapper memberMapper;

    /**
     * 分页查询会员
     *
     * @param couponQueryDTO
     * @return
     */
    @Override
    public Page<Member> pageMember(MemberQueryDTO couponQueryDTO) {
        return memberMapper.pageMember(getPage(), couponQueryDTO);
    }

    /**
     * 根据id查询会员
     *
     * @param id
     * @return
     */
    @Override
    public Member selectMemberById(Integer id) {
        return null;
    }

    /**
     * 导入会员信息
     *
     * @param inputStream
     */
    @Override
    public void importMember(InputStream inputStream) throws IOException, ClassNotFoundException {
        // 导入
//        ExcelUtils<MemberVO> excelUtils = new ExcelUtils<>(MemberVO.class);
//        List<MemberVO> memberVOS = excelUtils.importExcel(inputStream, "会员表");
//        for (MemberVO memberVO : memberVOS) {
//            Member member = new Member();
//            BeanUtil.copyProperties(memberVO, member);
//            memberMapper.insert(member);
//        }
        // 分段导入
        ExcelImportHandler excelImportHandler = new ExcelImportHandler<MemberVO>() {
            @Override
            public void handle(List<MemberVO> data) {
                for (MemberVO memberVO : data) {
                    Member member = new Member();
                    BeanUtil.copyProperties(memberVO, member);
                    memberMapper.insert(member);
                }
            }
        };
        ExcelUtils<MemberVO> excelUtils2 = new ExcelUtils<>(MemberVO.class, excelImportHandler);
        excelUtils2.importExcelSegment(inputStream);
    }

    /**
     * 导出会员信息
     *
     * @param params
     */
    @Override
    public String exportMember(MemberQueryDTO params) throws ClassNotFoundException {
        // 导出
//        List<Member> list = this.list();
//        List<MemberVO> memberVOS = BeanUtil.copyToList(list, MemberVO.class);
//        ExcelUtils<MemberVO> excelUtils = new ExcelUtils<>(MemberVO.class);
//        return excelUtils.exportExcel(System.currentTimeMillis() + ".xlsx", "会员表", memberVOS);
        // 分段导出
        ExcelExportHandler excelExportHandler = new ExcelExportHandler() {
            @Override
            public List getData(int current) {
                Page page = new Page(current, 10);
                Page<Member> memberPage = memberMapper.selectPage(page, Wrappers.lambdaQuery());
                return BeanUtil.copyToList(memberPage.getRecords(), MemberVO.class);
            }
        };
        ExcelUtils<MemberVO> excelUtils = new ExcelUtils<>(MemberVO.class, excelExportHandler);
        return excelUtils.exportExcelSegment(System.currentTimeMillis() + ".xlsx", "会员表");
    }
}
