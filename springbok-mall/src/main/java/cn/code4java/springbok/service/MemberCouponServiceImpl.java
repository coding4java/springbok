package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.MemberCouponCreateDTO;
import cn.code4java.springbok.entity.MemberCoupon;
import cn.code4java.springbok.mapper.MemberCouponMapper;
import cn.code4java.springbok.utils.CodeUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName MemberCouponServiceImpl
 * @Description: 会员券服务类
 * @Author zhonghengtech
 * @Date 2024/6/5
 * @Version V1.0
 **/
@Service
@AllArgsConstructor
public class MemberCouponServiceImpl extends BaseServiceImpl<MemberCouponMapper, MemberCoupon> implements MemberCouponService {

    @Override
    public boolean createMemberCoupon(MemberCouponCreateDTO memberCouponCreateDTO) {
        int num = memberCouponCreateDTO.getNum() == null ? 1 : memberCouponCreateDTO.getNum();
        List<MemberCoupon> memberCouponList = new LinkedList<>();
        for (int i = 0; i < num; i++) {
            MemberCoupon memberCoupon = new MemberCoupon();
            memberCoupon.setCouponNo(CodeUtils.generateCouponCode());
            memberCoupon.setMemberId(memberCouponCreateDTO.getMemberId());
            memberCoupon.setCouponId(memberCouponCreateDTO.getCouponId());
            memberCoupon.setStartDate(memberCouponCreateDTO.getStartDate());
            memberCoupon.setEndDate(memberCouponCreateDTO.getEndDate());
            memberCoupon.setCouponStatus(1);
            memberCouponList.add(memberCoupon);
        }
        return this.saveBatch(memberCouponList);
    }
}
