package cn.code4java.springbok.mapper;

import cn.code4java.springbok.dto.CouponDirectQueryDTO;
import cn.code4java.springbok.entity.CouponDirect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName CouponDirectMapper
 * @Description: CouponDirectMapper
 * @Author zhonghengtech
 * @Date 2024/6/1
 * @Version V1.0
 **/
public interface CouponDirectMapper extends BaseMapper<CouponDirect> {
    Page<CouponDirect> pageCouponDirect(Page page, @Param(value = "query") CouponDirectQueryDTO couponDirectQueryDTO);
}
