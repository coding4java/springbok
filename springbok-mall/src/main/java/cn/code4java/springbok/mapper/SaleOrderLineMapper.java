package cn.code4java.springbok.mapper;

import cn.code4java.springbok.entity.SaleOrderLine;
import cn.code4java.springbok.vo.SaleOrderLineVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @ClassName SaleOrderLineMapper
 * @Description: SaleOrderLineMapper
 * @Author fengwensheng
 * @Date 2024/02/21
 * @Version V1.0
 **/
public interface SaleOrderLineMapper extends BaseMapper<SaleOrderLine> {
    List<SaleOrderLineVO> listBySaleOrderId(Integer saleOrderId);
}
