package cn.code4java.springbok.mapper;

import cn.code4java.springbok.entity.Item;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @ClassName ItemMapper
 * @Description: Mapper类
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
public interface ItemMapper extends BaseMapper<Item> {
}
