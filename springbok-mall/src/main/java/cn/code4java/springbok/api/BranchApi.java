package cn.code4java.springbok.api;

import cn.code4java.springbok.dto.SysBranchQueryDTO;
import cn.code4java.springbok.entity.SysBranch;
import cn.code4java.springbok.service.SysBranchService;
import cn.code4java.springbok.vo.BaseResponse;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName BranchApi
 * @Description: 门店API
 * @Author xiehuangbao
 * @Date 2024/6/18
 * @Version V2.0.0
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/branch")
public class BranchApi {

    private final SysBranchService sysBranchService;

    /**
     * 分页查询销售商品
     *
     * @param sysBranchQueryDTO
     * @return
     */
    @GetMapping("/pageSysBranch")
    public BaseResponse pageSysBranch(SysBranchQueryDTO sysBranchQueryDTO) {
        return BaseResponse.success(sysBranchService.pageSysBranch(sysBranchQueryDTO));
    }

    /**
     * 获取默认门店
     *
     * @return
     */
    @GetMapping("/selectDefaultBranch")
    public BaseResponse selectDefaultBranch() {
        SysBranch sysBranch = sysBranchService.getOne(Wrappers.<SysBranch>lambdaQuery().eq(SysBranch::getBranchStatus, 1).last(" limit 1"));
        return BaseResponse.success(sysBranch);
    }
}
