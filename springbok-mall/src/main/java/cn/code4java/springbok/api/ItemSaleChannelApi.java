package cn.code4java.springbok.api;

import cn.code4java.springbok.dto.ItemSaleChannelQueryDTO;
import cn.code4java.springbok.entity.ItemSaleChannel;
import cn.code4java.springbok.service.api.ItemSaleChannelApiService;
import cn.code4java.springbok.vo.BaseResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ItemSaleChannelApi
 * @Description: 渠道商品API
 * @Author fengwensheng
 * @Date 2024/6/18
 * @Version V2.0.0
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/itemSaleChannel")
public class ItemSaleChannelApi {

    private final ItemSaleChannelApiService itemSaleChannelApiService;

    /**
     * 分页查询渠道商品
     *
     * @param itemSaleChannelQueryDTO
     * @return
     */
    @GetMapping("/pageItemSaleChannel")
    public BaseResponse pageItemSaleChannel(ItemSaleChannelQueryDTO itemSaleChannelQueryDTO) {
        return BaseResponse.success(itemSaleChannelApiService.pageItemSaleChannel(itemSaleChannelQueryDTO));
    }

    /**
     * 根据id查询渠道商品
     *
     * @param itemSaleChannel
     * @return
     */
    @GetMapping("/selectItemSaleChannelById")
    public BaseResponse selectItemSaleChannelById(ItemSaleChannel itemSaleChannel) {
        return BaseResponse.success(itemSaleChannelApiService.selectItemSaleChannelById(itemSaleChannel.getItemSaleChannelId()));
    }
}
