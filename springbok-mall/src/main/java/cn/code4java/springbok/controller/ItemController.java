package cn.code4java.springbok.controller;

import cn.code4java.springbok.entity.Item;
import cn.code4java.springbok.service.ItemService;
import cn.code4java.springbok.vo.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * @ClassName ItemController
 * @Description: 基础商品
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
@Tag(name = "基础商品")
@RestController
@RequiredArgsConstructor
@RequestMapping("/item")
public class ItemController {

    private final ItemService itemService;

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @GetMapping("/pageItem")
    @Operation(summary = "分页查询", description = "分页查询")
    public BaseResponse pageItem(Item params) {
        return BaseResponse.success(itemService.pageItem(params));
    }

    /**
     * 新增
     *
     * @param item
     * @return
     */
    @PostMapping("/addItem")
    public BaseResponse addItem(@RequestBody Item item) {
        return BaseResponse.success(itemService.save(item));
    }

    /**
     * 修改
     *
     * @param item
     * @return
     */
    @PostMapping("/updateItem")
    public BaseResponse updateItem(@RequestBody Item item) {
        return BaseResponse.success(itemService.updateById(item));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteItem")
    public BaseResponse deleteItem(@Parameter(description = "id", required = true) Integer id) {
        return BaseResponse.success(itemService.removeById(id));
    }
}
