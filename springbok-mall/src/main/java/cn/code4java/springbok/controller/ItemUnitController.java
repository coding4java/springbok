package cn.code4java.springbok.controller;

import cn.code4java.springbok.entity.ItemUnit;
import cn.code4java.springbok.service.ItemUnitService;
import cn.code4java.springbok.vo.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * @ClassName ItemUnitController
 * @Description: 基本单位
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
@Tag(name = "基本单位")
@RestController
@RequiredArgsConstructor
@RequestMapping("/itemUnit")
public class ItemUnitController {

    private final ItemUnitService itemUnitService;

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @GetMapping("/pageItemUnit")
    @Operation(summary = "分页查询", description = "分页查询")
    public BaseResponse pageItemUnit(ItemUnit params) {
        return BaseResponse.success(itemUnitService.pageItemUnit(params));
    }

    /**
     * 列表查询
     *
     * @param params
     * @return
     */
    @GetMapping("/listItemUnit")
    @Operation(summary = "列表查询", description = "列表查询")
    public BaseResponse listItemUnit(ItemUnit params) {
        return BaseResponse.success(itemUnitService.list());
    }

    /**
     * 新增
     *
     * @param itemUnit
     * @return
     */
    @PostMapping("/addItemUnit")
    public BaseResponse addItemUnit(@RequestBody ItemUnit itemUnit) {
        return BaseResponse.success(itemUnitService.save(itemUnit));
    }

    /**
     * 修改
     *
     * @param itemUnit
     * @return
     */
    @PostMapping("/updateItemUnit")
    public BaseResponse updateItemUnit(@RequestBody ItemUnit itemUnit) {
        return BaseResponse.success(itemUnitService.updateById(itemUnit));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteItemUnit")
    public BaseResponse deleteItemUnit(@Parameter(description = "id", required = true) Integer id) {
        return BaseResponse.success(itemUnitService.removeById(id));
    }
}
