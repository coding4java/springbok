package cn.code4java.springbok.utils.excel;

import java.util.List;

/**
 * @ClassName ExcelExportHandler
 * @Description: TODO
 * @Author fengwensheng
 * @Date 2024/7/6
 * @Version V2.0.0
 **/
public interface ExcelExportHandler<T> {
    /**
     * 获取数据
     *
     * @param current 当前页码
     * @return
     */
    List<T> getData(int current);
}
