package cn.code4java.springbok.utils.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import java.util.List;

/**
 * @ClassName ExcelListener
 * @Description: TODO
 * @Author fengwensheng
 * @Date 2024/7/6
 * @Version V2.0.0
 **/
public class ExcelListener<T> implements ReadListener<T> {

    /**
     * 分段大小，默认100条，然后清理list，方便内存回收
     */
    private static final int BATCH_COUNT = 100;
    /**
     * 缓存的数据
     */
    private List<T> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    /**
     * 分段导入处理器
     */
    private ExcelImportHandler excelImportHandler;

    public ExcelListener(ExcelImportHandler excelImportHandler) {
        this.excelImportHandler = excelImportHandler;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(T data, AnalysisContext context) {
        cachedDataList.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (cachedDataList.size() >= BATCH_COUNT) {
            this.excelImportHandler.handle(cachedDataList);
            // 调用后清理list
            cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
        }
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 每个sheet完成后都会调用，处理最后一批未达到BATCH_COUNT数量的数据
        this.excelImportHandler.handle(cachedDataList);
        cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    }
}
