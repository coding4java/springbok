package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.Table;
import cn.code4java.springbok.entity.TableColumn;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import java.lang.String;
import java.lang.Integer;
import java.util.List;

/**
 * @ClassName TableVO
 * @Description: 生成表
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
@Data
public class TableVO extends Table {

    /**
     * 字段列表
     */
    private List<TableColumn> tableColumns;
}
