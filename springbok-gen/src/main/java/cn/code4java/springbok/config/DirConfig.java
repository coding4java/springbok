package cn.code4java.springbok.config;

import cn.code4java.springbok.enums.ModeEnum;

/**
 * @ClassName DirConfig
 * @Description: TODO
 * @Author fengwensheng
 * @Date 2024/7/10
 * @Version V2.0.0
 **/
public class DirConfig {
    private String dirName;
    private String fileNameMode;

    private DirConfig(Builder builder) {
        this.dirName = builder.dirName;
        this.fileNameMode = builder.fileNameMode;
    }

    public String getDirName() {
        return dirName;
    }

    public String getFileNameMode() {
        return fileNameMode;
    }

    public static class Builder {
        private String dirName;
        private String fileNameMode = ModeEnum.PASCAL_CASE.name();

        public Builder dirName(String dirName) {
            this.dirName = dirName;
            return this;
        }

        public Builder fileNameMode(String fileNameMode) {
            this.fileNameMode = fileNameMode;
            return this;
        }

        public DirConfig build() {
            return new DirConfig(this);
        }
    }
}
