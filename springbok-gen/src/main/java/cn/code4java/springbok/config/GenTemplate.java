package cn.code4java.springbok.config;

/**
 * @ClassName GenTemplate
 * @Description: 生成模板
 * @Author fengwensheng
 * @Date 2024/7/8
 * @Version V2.0.0
 **/
public class GenTemplate {
    /**
     * 模板路径
     */
    private String path;
    /**
     * 模板名称
     */
    private String name;
    /**
     * 生成路径
     */
    private String outPath;
    /**
     * 模板目录
     */
    private DirConfig dirConfig;

    private GenTemplate(Builder builder) {
        this.path = builder.path;
        this.name = builder.name;
        this.outPath = builder.outPath;
        this.dirConfig = builder.dirConfig;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public String getOutPath() {
        return outPath;
    }

    public DirConfig getDirConfig() {
        return dirConfig;
    }

    public static class Builder {
        private String path;
        private String name;
        private String outPath;
        private DirConfig dirConfig;

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder outPath(String outPath) {
            this.outPath = outPath;
            return this;
        }

        public Builder dirConfig(DirConfig dirConfig) {
            this.dirConfig = dirConfig;
            return this;
        }

        public GenTemplate build() {
            return new GenTemplate(this);
        }
    }
}
