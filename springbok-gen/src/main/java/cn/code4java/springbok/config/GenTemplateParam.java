package cn.code4java.springbok.config;

import cn.code4java.springbok.entity.TableColumn;
import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @ClassName GenTable
 * @Description: TODO
 * @Author fengwensheng
 * @Date 2024/7/8
 * @Version V2.0.0
 **/
@Data
public class GenTemplateParam {

    /**
     * 表名
     */
    private String tableName;
    /**
     * 类名
     */
    private String className;
    /**
     * 包名
     */
    private String packageName;
    /**
     * 描述
     */
    private String description;
    /**
     * 作者
     */
    private String author;
    /**
     * 创建日期
     */
    private String date;
    /**
     * 版本号
     */
    private String version;
    /**
     * 表字段列表
     */
    private List<TableColumn> tableColumnList;
    /**
     * 导入类型集合
     */
    private Set<String> importTypeSet;
}
