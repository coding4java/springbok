package cn.code4java.springbok.enums;

/**
 * @ClassName JavaSQLTypeMapEnum
 * @Description: 生成模板枚举
 * @Author fengwensheng
 * @Date 2024/7/8
 * @Version V2.0.0
 **/
public enum GenTemplateEnum {

    /**
     * entity类模板
     */
    TEMPLATE_MYSQL("mysql.sql.vm", "", "sql", "sql"),
    /**
     * entity类模板
     */
    TEMPLATE_ENTITY("entity.java.vm", "", "java", "entity"),
    /**
     * dto类模板
     */
    TEMPLATE_DTO("dto.java.vm", "DTO", "java", "dto"),
    /**
     * vo类模板
     */
    TEMPLATE_VO("vo.java.vm", "VO", "java", "vo"),
    /**
     * controller类模板
     */
    TEMPLATE_CONTROLLER("controller.java.vm", "Controller", "java", "controller"),
    /**
     * service类模板
     */
    TEMPLATE_SERVICE("service.java.vm", "Service", "java", "service"),
    /**
     * serviceImpl类模板
     */
    TEMPLATE_SERVICE_IMPL("serviceImpl.java.vm", "ServiceImpl", "java", "service"),
    /**
     * mapper类模板
     */
    TEMPLATE_MAPPER("mapper.java.vm", "Mapper", "java", "mapper"),
    /**
     * mapper xml类模板
     */
    TEMPLATE_MAPPER_XML("mapperXml.xml.vm", "Mapper", "xml", "mapper"),
    /**
     * 前端vue模板
     */
    TEMPLATE_VUE3("vue3.vue.vm", "", "vue", "views"),
    /**
     * 前端api模板
     */
    TEMPLATE_API_JS("api.js.vm", "", "js", "api");

    private String template;
    private String suffix;
    private String type;
    private String dir;

    GenTemplateEnum(String template, String suffix, String type, String dir) {
        this.template = template;
        this.suffix = suffix;
        this.type = type;
        this.dir = dir;
    }

    public String getTemplate() {
        return template;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getType() {
        return type;
    }

    public String getDir() {
        return dir;
    }

    public static GenTemplateEnum getGenTemplateEnum(String template) throws Exception {
        for (GenTemplateEnum genTemplateEnum : GenTemplateEnum.values()) {
            if (genTemplateEnum.getTemplate().equals(template)) {
                return genTemplateEnum;
            }
        }
        throw new Exception("生成模板不存在");
    }
}
