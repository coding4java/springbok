package cn.code4java.springbok.mapper;

import cn.code4java.springbok.entity.Table;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @ClassName TableMapper
 * @Description: Mapper类
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
public interface TableMapper extends BaseMapper<Table> {
}
