package cn.code4java.springbok.dto;

import cn.code4java.springbok.entity.Table;
import cn.code4java.springbok.entity.TableColumn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.lang.String;
import java.lang.Integer;
import java.util.List;

/**
 * @ClassName TableDTO
 * @Description: 生成表
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
@Data
public class TableDTO extends Table {

    /**
     * 字段列表
     */
    private List<TableColumn> tableColumns;
}
