package cn.code4java.springbok.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import java.lang.Boolean;
import java.lang.String;
import java.lang.Integer;

/**
 * @ClassName TableColumnDTO
 * @Description: 生成表字段
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
@Data
@Schema(title = "查询参数")
public class TableColumnDTO {

    /**
     * id
     */
    @Schema(title = "id")
    private Integer tableColumnId;
    /**
     * 表id
     */
    @Schema(title = "表id")
    private Integer tableId;
    /**
     * 列名
     */
    @Schema(title = "列名")
    private String columnName;
    /**
     * 列类型
     */
    @Schema(title = "列类型")
    private String columnType;
    /**
     * 列长度
     */
    @Schema(title = "列长度")
    private Integer columnLength;
    /**
     * 字段名
     */
    @Schema(title = "字段名")
    private String fieldName;
    /**
     * 字段类型
     */
    @Schema(title = "字段类型")
    private String fieldType;
    /**
     * 列描述
     */
    @Schema(title = "列描述")
    private String columnComment;
    /**
     * 是否主键
     */
    @Schema(title = "是否主键")
    private Boolean primaryKey;
    /**
     * 是否为空
     */
    @Schema(title = "是否为空")
    private Boolean isNull;

    /**
     * 页大小
     */
    @Schema(title = "页大小")
    private long size;

    /**
     * 页码
     */
    @Schema(title = "页码")
    private long current;
}
