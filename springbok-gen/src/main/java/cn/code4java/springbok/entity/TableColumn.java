package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName TableColumn
 * @Description: 列
 * @Author fengwensheng
 * @Date 2024/7/8
 * @Version V2.0.0
 **/
@Data
@TableName(value = "sys_table_column")
public class TableColumn {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer tableColumnId;
    /**
     * 表id
     */
    private Integer tableId;
    /**
     * 列名
     */
    private String columnName;
    /**
     * 列类型
     */
    private String columnType;
    /**
     * 列长度
     */
    private Integer columnLength;
    /**
     * 小数长度
     */
    private Integer decimalLength;
    /**
     * 字段名
     */
    private String fieldName;
    /**
     * 字段类型
     */
    private String fieldType;
    /**
     * 列描述
     */
    private String columnComment;
    /**
     * 是否主键
     */
    private Boolean primaryKey;
    /**
     * 是否自增
     */
    private Boolean autoIncrement;
    /**
     * 是否为空
     */
    private Boolean nullable;
    /**
     * 默认值
     */
    private String defaultValue;
}
