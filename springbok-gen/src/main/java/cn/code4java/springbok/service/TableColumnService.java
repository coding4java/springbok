package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.TableColumnDTO;
import cn.code4java.springbok.entity.TableColumn;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ClassName TableColumnService
 * @Description: 服务类
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
public interface TableColumnService extends IService<TableColumn> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    Page<TableColumn> pageTableColumn(TableColumnDTO params);
}
