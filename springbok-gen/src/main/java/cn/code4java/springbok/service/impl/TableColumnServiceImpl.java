package cn.code4java.springbok.service.impl;

import cn.code4java.springbok.dto.TableColumnDTO;
import cn.code4java.springbok.entity.TableColumn;
import cn.code4java.springbok.mapper.TableColumnMapper;
import cn.code4java.springbok.service.BaseServiceImpl;
import cn.code4java.springbok.service.TableColumnService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;

/**
 * @ClassName TableColumnServiceImpl
 * @Description: 服务实现类
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
@Service
public class TableColumnServiceImpl extends BaseServiceImpl<TableColumnMapper, TableColumn> implements TableColumnService {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @Override
    public Page<TableColumn> pageTableColumn(TableColumnDTO params) {
        return this.page(getPage());
    }
}
