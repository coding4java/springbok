package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName PurchaseInOrderQueryDTO
 * @Description: PurchaseInOrderQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class PurchaseInOrderQueryDTO extends BaseQueryDTO {

    /**
     * 订单状态
     * 1：制单
     * 2：审核
     */
    private Integer orderStatus;
    /**
     * 门店名称
     */
    private String branchName;
}
