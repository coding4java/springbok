package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName StockLineQueryDTO
 * @Description: StockLineQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class StockLineQueryDTO extends BaseQueryDTO {

    /**
     * 商品名称
     */
    private String itemName;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 订单类型
     * @see cn.code4java.springbok.enums.BillTypeEnum
     */
    private Integer orderType;
    /**
     * 库存id
     */
    private Integer stockId;
}
