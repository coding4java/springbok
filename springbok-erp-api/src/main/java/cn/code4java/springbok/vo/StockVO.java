package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.Stock;
import cn.code4java.springbok.entity.StockLine;
import lombok.Data;

import java.util.List;

/**
 * @ClassName StockVO
 * @Description: StockVO
 * @Author fengwensheng
 * @Date 2023/11/22
 * @Version V1.0
 **/
@Data
public class StockVO extends Stock {

    /**
     * 门店编码
     */
    private String branchCode;
    /**
     * 门店名称
     */
    private String branchName;
    /**
     * 商品编码
     */
    private String itemCode;
    /**
     * 商品名称
     */
    private String itemName;
    /**
     * 基本单位
     */
    private String itemUnitName;
    /**
     * 库存资料明细列表
     */
    private List<StockLine> stockLine;
}
