package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.InventoryOrderLine;
import lombok.Data;

/**
 * @ClassName InventoryOrderLineVO
 * @Description: InventoryOrderLineVO
 * @Author fengwensheng
 * @Date 2023/11/22
 * @Version V1.0
 **/
@Data
public class InventoryOrderLineVO extends InventoryOrderLine {
    /**
     * 商品名称
     */
    private String itemName;
    /**
     * 基础单位
     */
    private String itemUnitName;
}
