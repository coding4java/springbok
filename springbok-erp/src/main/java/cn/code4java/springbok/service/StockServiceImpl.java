package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.*;
import cn.code4java.springbok.entity.Stock;
import cn.code4java.springbok.entity.StockLine;
import cn.code4java.springbok.exception.BusinessException;
import cn.code4java.springbok.exception.ExceptionEnum;
import cn.code4java.springbok.mapper.StockLineMapper;
import cn.code4java.springbok.mapper.StockMapper;
import cn.code4java.springbok.vo.StockLineVO;
import cn.code4java.springbok.vo.StockVO;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName StockServiceImpl
 * @Description: 库存服务实现类
 * @Author fengwensheng
 * @Date 2023/11/21
 * @Version V1.0
 **/
@Service
@AllArgsConstructor
public class StockServiceImpl extends BaseServiceImpl<StockMapper, Stock> implements StockService {

    private StockMapper stockMapper;
    private StockLineMapper stockLineMapper;

    /**
     * 基础商品库存出入库
     *
     * @param stockInOutDTO
     * @return
     */
    @Override
    @Transactional
    public boolean inOutStock(StockInOutDTO stockInOutDTO) {
        stockInOutDTO.getStockInOutLines().stream().forEach(stockInOutLine -> {
            Stock stock = stockMapper.selectOne(new LambdaQueryWrapper<Stock>()
                    .eq(Stock::getSysBranchId, stockInOutDTO.getSysBranchId())
                    .eq(Stock::getItemCode, stockInOutLine.getItemCode()));
            if (stock == null) {
                //增加库存记录
                stock = new Stock();
                stock.setSysBranchId(stockInOutDTO.getSysBranchId());
                stock.setItemCode(stockInOutLine.getItemCode());
                stock.setQuantity(stockInOutLine.getQuantity());
                stockMapper.insert(stock);
            } else {
                //修改库存记录
                stock.setQuantity(stock.getQuantity().add(stockInOutLine.getQuantity()));
                stockMapper.updateById(stock);
            }
            //库存流水
            StockLine stockLine = new StockLine();
            stockLine.setStockId(stock.getStockId());
            stockLine.setOrderNo(stockInOutDTO.getOrderNo());
            stockLine.setItemCode(stockInOutLine.getItemCode());
            stockLine.setQuantity(stockInOutLine.getQuantity());
            stockLine.setOrderType(stockInOutDTO.getBillType());
            stockLineMapper.insert(stockLine);
        });
        return true;
    }

    /**
     * 分页查询库存
     *
     * @param stockQueryDTO
     * @return
     */
    @Override
    public Page<StockVO> pageStock(StockQueryDTO stockQueryDTO) {
        return stockMapper.pageStock(getPage(), stockQueryDTO);
    }

    /**
     * 查询库存列表
     *
     * @param stockDTO
     * @return
     */
    @Override
    public List<StockVO> listStock(StockDTO stockDTO) {
        return stockMapper.listStock(stockDTO);
    }

    /**
     * 根据id查询库存
     *
     * @param stockId
     * @return
     */
    @Override
    public StockVO selectStockById(int stockId) {
        StockVO stockVO = new StockVO();
        Stock stock = stockMapper.selectById(stockId);
        BeanUtils.copyProperties(stock, stockVO);
        List<StockLine> stockLines = stockLineMapper.selectList(new LambdaQueryWrapper<StockLine>().eq(StockLine::getStockId, stockId));
        stockVO.setStockLine(stockLines);
        return stockVO;
    }

    /**
     * 分页查询库存明细
     *
     * @param stockLineQueryDTO
     * @return
     */
    @Override
    public Page<StockLineVO> pageStockLine(StockLineQueryDTO stockLineQueryDTO) {
        return stockLineMapper.pageStockLine(getPage(), stockLineQueryDTO);
    }
}
