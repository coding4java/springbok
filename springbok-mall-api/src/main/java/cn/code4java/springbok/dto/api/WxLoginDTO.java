package cn.code4java.springbok.dto.api;

import lombok.Data;

/**
 * @ClassName WxLoginDTO
 * @Description: 微信登录
 * @Author fengwensheng
 * @Date 2024/3/14
 * @Version V1.0
 **/
@Data
public class WxLoginDTO {

    private String code;
    private String encryptedData;
    private String iv;
}
