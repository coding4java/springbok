package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName TagQueryDTO
 * @Description: TagQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class TagQueryDTO extends BaseQueryDTO {

    /**
     * 标签类型
     * 1：商品
     * 2：用户
     */
    private Integer tagType;
    /**
     * 标签名称
     */
    private String tagName;
}
