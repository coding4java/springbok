package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName BannerQueryDTO
 * @Description: BannerQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class BannerQueryDTO extends BaseQueryDTO {

    /**
     * 状态
     * 1：启用
     * 2：停用
     */
    private Integer status;
}
