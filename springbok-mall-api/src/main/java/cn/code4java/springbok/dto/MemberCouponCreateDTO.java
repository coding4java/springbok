package cn.code4java.springbok.dto;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName MemberCouponCreateDTO
 * @Description: 创建会员券DTO
 * @Author zhonghengtech
 * @Date 2024/6/5
 * @Version V1.0
 **/
@Data
public class MemberCouponCreateDTO {

    /**
     * 会员id
     */
    private Integer memberId;
    /**
     * 券id
     */
    private Integer couponId;
    /**
     * 开始日期
     */
    private Date startDate;
    /**
     * 结束日期
     */
    private Date endDate;
    /**
     * 创建数量，不传默认为1
     */
    private Integer num;
}
