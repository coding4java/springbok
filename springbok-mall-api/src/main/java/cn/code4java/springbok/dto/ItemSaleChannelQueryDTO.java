package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName ItemSaleChannelDTO
 * @Description: ItemSaleChannelDTO
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V1.0
 **/
@Data
public class ItemSaleChannelQueryDTO extends BaseQueryDTO {

    /**
     * 销售商品名称
     */
    private String itemSaleName;
    /**
     * 门店id
     */
    private Integer sysBranchId;
    /**
     * 门店名称
     */
    private String branchName;
    /**
     * 销售渠道
     */
    private Integer saleChannel;
}
