package cn.code4java.springbok.dto;

import cn.code4java.springbok.entity.BaseEntitySerializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName BaseQueryDTO
 * @Description: 基础查询DTO
 * @Author fengwensheng
 * @Date 2024/7/5
 * @Version V2.0.0
 **/
@Data
public class BaseQueryDTO extends BaseEntitySerializable {

    /**
     * 页大小
     */
    @Schema(title = "页大小")
    private long size;

    /**
     * 页码
     */
    @Schema(title = "页码")
    private long current;
}
