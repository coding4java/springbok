package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName MemberQueryDTO
 * @Description: MemberQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class MemberQueryDTO extends BaseQueryDTO {

    /**
     * 用户名
     */
    private String username;
    /**
     * 会员手机号
     */
    private String phone;
}
