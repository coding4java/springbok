package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName CouponQueryDTO
 * @Description: CouponQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class CouponQueryDTO extends BaseQueryDTO {

    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券类型
     * 1：满减
     * 2：折扣
     */
    private Integer couponType;
}
