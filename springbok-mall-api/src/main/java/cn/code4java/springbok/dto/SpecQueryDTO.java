package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName SpecQueryDTO
 * @Description: SpecQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class SpecQueryDTO extends BaseQueryDTO {

    /**
     * 规格名称
     */
    private String specName;
}
