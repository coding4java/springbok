package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.CouponDirectDTO;
import cn.code4java.springbok.dto.CouponDirectQueryDTO;
import cn.code4java.springbok.entity.CouponDirect;
import cn.code4java.springbok.vo.CouponDirectVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @ClassName CouponDirectService
 * @Description: 定向发券服务类
 * @Author zhonghengtech
 * @Date 2024/6/1
 * @Version V1.0
 **/
public interface CouponDirectService {
    /**
     * 分页查询定向发券
     *
     * @param couponDirectQueryDTO
     * @return
     */
    Page<CouponDirect> pageCouponDirect(CouponDirectQueryDTO couponDirectQueryDTO);

    /**
     * 根据id查询优惠券
     *
     * @param id
     * @return
     */
    CouponDirectVO selectCouponDirectById(Integer id);

    /**
     * 新增定向发券
     *
     * @param couponDirectDTO
     * @return
     */
    boolean addCouponDirect(CouponDirectDTO couponDirectDTO);

    /**
     * 修改定向发券
     *
     * @param couponDirectDTO
     * @return
     */
    boolean updateCouponDirect(CouponDirectDTO couponDirectDTO);

    /**
     * 删除定向发券
     *
     * @param id
     * @return
     */
    boolean deleteCouponDirect(Integer id);

    /**
     * 发券
     *
     * @param couponDirectId
     * @return
     */
    boolean sendCoupon(Integer couponDirectId);
}
