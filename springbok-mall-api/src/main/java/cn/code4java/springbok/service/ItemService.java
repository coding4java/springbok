package cn.code4java.springbok.service;

import cn.code4java.springbok.entity.Item;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ClassName ItemService
 * @Description: 服务类
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
public interface ItemService extends IService<Item> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    Page<Item> pageItem(Item params);
}
