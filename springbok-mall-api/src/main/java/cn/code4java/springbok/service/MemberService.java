package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.MemberQueryDTO;
import cn.code4java.springbok.entity.Member;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.IOException;
import java.io.InputStream;

/**
 * @ClassName MemberService
 * @Description: 会员服务类
 * @Author fengwensheng
 * @Date 2024/1/27
 * @Version V1.0
 **/
public interface MemberService {
    /**
     * 分页查询会员
     *
     * @param memberQueryDTO
     * @return
     */
    Page<Member> pageMember(MemberQueryDTO memberQueryDTO);

    /**
     * 根据id查询会员
     *
     * @param id
     * @return
     */
    Member selectMemberById(Integer id);

    /**
     * 导入会员信息
     *
     * @param inputStream
     */
    void importMember(InputStream inputStream) throws IOException, ClassNotFoundException;

    /**
     * 导出会员信息
     *
     * @param params
     */
    String exportMember(MemberQueryDTO params) throws ClassNotFoundException;
}
