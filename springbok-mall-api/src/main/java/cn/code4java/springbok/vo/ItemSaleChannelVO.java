package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.ItemSale;
import cn.code4java.springbok.entity.ItemSaleChannel;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName ItemSaleChannelVO
 * @Description: 渠道商品VO
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V1.0
 **/
@Data
public class ItemSaleChannelVO extends ItemSaleChannel {

    /**
     * 门店编码
     */
    private String branchCode;
    /**
     * 门店名称
     */
    private String branchName;
    /**
     * 销售商品编码
     */
    private String itemSaleCode;

    /**
     * 销售商品名称
     */
    private String itemSaleName;

    /**
     * 销售价格
     */
    private BigDecimal itemSalePrice;

    /**
     * 销售商品描述
     */
    private String itemSaleDescription;

    /**
     * 营销分类编码
     */
    private String itemSaleClassCode;

    /**
     * 标签数组
     */
    private String tagIds;

    /**
     * 商品主图
     */
    private String mainImage;

    /**
     * 商品主图列表
     */
    private List<String> mainImageList;

    /**
     * 营销分类名称
     */
    private String itemSaleClassName;

    /**
     * 销售商品规格列表
     */
    private List<ItemSaleChannelSkuVO> itemSaleSkuList;

    /**
     * 规格json
     */
    private String specJson;

    /**
     * 规格列表
     */
    @TableField(exist = false)
    private List<ItemSale.Spec> specList;

    @Data
    public static class Spec {
        /**
         * 规格名
         */
        private String specName;
        /**
         * 规格项
         */
        private List<String> specValueList;
    }

    /**
     * 属性json
     */
    private String propertyJson;

    /**
     * 属性列表
     */
    @TableField(exist = false)
    private List<ItemSale.Property> propertyList;

    @Data
    public static class Property {
        /**
         * 属性名
         */
        private String propertyName;
        /**
         * 属性值
         */
        private String propertyValue;
    }
}
