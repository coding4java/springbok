package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.ItemSaleSku;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName ItemSaleChannelVO
 * @Description: 渠道商品详情VO
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V1.0
 **/
@Data
public class ItemSaleChannelSkuVO extends ItemSaleSku {

    /**
     * 库存
     */
    private BigDecimal quantity;
    /**
     * 渠道商品名称
     */
    private String itemSaleChannelName;
}
