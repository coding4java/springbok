package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName CouponDirect
 * @Description: 定向发券
 * @Author zhonghengtech
 * @Date 2024/6/1
 * @Version V1.0
 **/
@Data
@TableName(value = "mall_coupon_direct")
public class CouponDirect extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer couponDirectId;
    /**
     * 发券名称
     */
    private String couponDirectName;
    /**
     * 发放类型 1 按标签 2 指定手机号
     */
    private Integer sendType;
    /**
     * 发放目标id
     */
    private String sendTargetIds;
    /**
     * 是否启用短信通知 0 否 1 是
     */
    private Integer smsNotify;
    /**
     * 状态 0 未开始 1 发送中 2 已完成 3 发送失败
     */
    private Integer status;
}
