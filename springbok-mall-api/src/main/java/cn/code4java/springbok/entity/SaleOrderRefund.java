package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName SaleOrderRefund
 * @Description: 售后订单
 * @Author fengwensheng
 * @Date 2023/11/21
 * @Version V1.0
 **/
@Data
@TableName(value = "mall_sale_order_refund")
public class SaleOrderRefund extends BaseEntity {

    /**
     * 售后订单id
     */
    @TableId(type = IdType.AUTO)
    private Integer saleOrderRefundId;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 售后单号
     */
    private String refundNo;
    /**
     * 售后类型
     * 1：仅退款
     * 2：退货退款
     */
    private Integer refundType;
    /**
     * 售后状态
     * 1：未审核
     * 2：已完成
     */
    private Integer refundStatus;
    /**
     * 售后原因编码
     */
    private String refundReasonCode;
    /**
     * 售后原因
     */
    private String refundReasonText;
    /**
     * 用户申请原因
     */
    private String applyReason;
}
