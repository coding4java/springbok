package cn.code4java.springbok.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName SysPostQueryDTO
 * @Description: SysPostQueryDTO
 * @Author fengwensheng
 * @Date 2024/03/13
 * @Version V1.0
 **/
@Data
@Schema(title = "岗位查询参数")
public class SysPostQueryDTO extends BaseQueryDTO {

    /**
     * 岗位名称
     */
    @Schema(title = "岗位名称")
    private String sysPostName;
}
