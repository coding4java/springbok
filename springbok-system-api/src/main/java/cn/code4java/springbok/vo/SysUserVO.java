package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName SysUserVO
 * @Description: SysUserVO
 * @Author fengwensheng
 * @Date 2024/3/13
 * @Version V1.0
 **/
@Data
public class SysUserVO extends BaseEntity {
    /**
     * 用户id
     */
    @TableId(type = IdType.AUTO)
    @Schema(title = "用户id")
    private Integer sysUserId;
    /**
     * 用户名
     */
    @Schema(title = "用户名", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;
    /**
     * 密码
     */
    @Schema(title = "密码", requiredMode = Schema.RequiredMode.REQUIRED)
    private String userPassword;
    /**
     * 昵称
     */
    @Schema(title = "昵称", requiredMode = Schema.RequiredMode.REQUIRED)
    private String nickName;
    /**
     * 手机号
     */
    @Schema(title = "手机号")
    private String mobile;
    /**
     * 角色id
     */
    @Schema(title = "角色id", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer sysRoleId;
    /**
     * 岗位id
     */
    @Schema(title = "岗位id")
    private String sysPostId;
    /**
     * 岗位名称
     */
    @Schema(title = "岗位名称")
    private String sysPostName;
}
