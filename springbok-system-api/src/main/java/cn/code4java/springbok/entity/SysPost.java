package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName SysPost
 * @Description: 岗位
 * @Author fengwensheng
 * @Date 2024/3/13
 * @Version V1.0
 **/
@Data
@Schema(title = "岗位实体")
@TableName(value = "sys_post")
public class SysPost extends BaseEntity {

    /**
     * 岗位id
     */
    @TableId(type = IdType.AUTO)
    @Schema(title = "岗位id")
    private Integer sysPostId;
    /**
     * 岗位编码
     */
    @Schema(title = "岗位编码", requiredMode = Schema.RequiredMode.REQUIRED)
    private String sysPostCode;
    /**
     * 岗位名称
     */
    @Schema(title = "岗位名称", requiredMode = Schema.RequiredMode.REQUIRED)
    private String sysPostName;
    /**
     * 备注
     */
    @Schema(title = "备注")
    private String remark;
    /**
     * 排序
     */
    @Schema(title = "排序")
    private Integer postSort;
}
