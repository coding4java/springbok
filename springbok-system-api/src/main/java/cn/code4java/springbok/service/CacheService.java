package cn.code4java.springbok.service;

import cn.code4java.springbok.vo.CecheIndexVO;

/**
 * @ClassName CacheService
 * @Description: 缓存服务类
 * @Author fengwensheng
 * @Date 2024/8/2
 * @Version V2.0.3
 **/
public interface CacheService {
    /**
     * 查询缓存指标
     *
     * @return
     */
    CecheIndexVO listCacheIndex();
}
